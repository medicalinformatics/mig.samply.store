# Samply Store

Samply Store is a *generic* data access layer library, that provides access to
a PostgreSQL 9.3 database. It uses the JSON datatype feature introduced in
PostgreSQL 9.2 and is therefore incompatible with other relational databases.
Ir requires PostgreSQL >=9.3 because it uses the JSON accessor functions
introduced in 9.3.

# Features

- build your own database scheme using XML
- one class to access all tables: `Resource` (exception: transactions)
- generic properties: add more properties to a `Resource` using `addProperty`,
  no scheme changes necessary. The library uses PostgreSQLs `JSON` datatype
  feature to store your generic properties.
- all tables are versioned: access all changes of one specific `Resource`.
- generic interface for permission management: implement your own `AccessController`
  to meet your requirements.


# Introduction

In contrast to the DAO pattern, this library uses only one class for every
table in the database: `Resource`. A `Resource` is comparable to one row in a table.

An example:

```
Table user

-------------------------------------------------------
|   id        |  name             |  data             |
-------------------------------------------------------
|   3         |  Mark             | {"age":34}        |
|   4         |  Stefan           | {"age":14}        |
-------------------------------------------------------
```

If you use Samply Store to get the user with the ID 3, the library returns a
`Resource` that has two properties: `name` and `age`. The value of the `name`
property is a `StringLiteral` whose String representation is `Mark`. The value
of the `age` property is a `NumberLiteral` whose String representation is `4`.


# Build

Use maven to build the `jar` file:

```
mvn clean package
```

Use it as a dependency:

```xml
<dependency>
    <groupId>de.mig.samply</groupId>
    <artifactId>store</artifactId>
    <version>${version}</version>
</dependency>
```



# Design

The idea behind Samply.Store is, that the database structure does not need to change, if there
are any new attributes to store. This key design choice is possible via the JSON datatype
from PostgreSQL, that was introduced in PostgreSQL 9.3. This datatype makes storing plain
JSON object possible, but also introduces way to use that JSON object in SQL queries, e.g.
the query `SELECT data#>>'{name}' FROM users` selects the names of all users.

Furthermore Samply.Store is not hardcoded to know what entities and relationships you want to store.
It is configured via an XML file, that defines the entities, the relationships, and the *known*
attributes. From this XML file Samply Store create all SQL statements that are necessary
to create the database structure. The entities that have a dynamic set of attributes are called resources.

Those known attributes are stored in a separate column. All the other attributes
are stored in the JSON `data` column. Relationships are always expressed as separate columns (1:n) or
separate tables (n:m).

For each resource there is a history table, and every time a resource is changed, the old version
of the resource and all its relationships (for n:m) are stored in the history table.

For access rights, Samply.Store offers an interface, the access controller interface.
This interface is called for every method, that returns a resource, e.g. `getResources`.
The controller must ensure, that the current user can only access those resources, that he
has access to. This can be done via additional `WHERE` clauses, e.g. `WHERE patient.id IN (SELECT ...)`.
Samply.Store does not offer a standard controller implementation, infact it is always recommended to
implement several classes and interfaces, see `Samply.Store.OSSE`, which implements
the controller interface among other classes and interfaces.


## Resource Queries

Samply.Store offers a way to query for resources via a `ResourceQuery`. The result of
this resource query is always a list of resources, e.g. "a list of patients". Those patients
meet the criterias defined in the resource query, e.g. the query "Give me all patients with the name 'Paul'"
could be implemented like this:

```
ResourceQuery query = new ResourceQuery("patient");
query.addCriteria("name", "Paul");
```

The query "Give me all patients, that have a case created on '01.04.2015'" can be implemented like this:

```
ResourceQuery query = new ResourceQuery("patient");
query.addCriteria("case", "createdOn", new TimestampLiteral(...));
```

Samply.Store will automatically try to connect the entity patient with the entity case through
the relationships it has knowledge of through the XML configuration file. For this it will always choose
the *shortest* relationship.


## SQL Layer

Samply.Store uses its own SQL layer, which makes handling complex SQL queries easier.


## Samply.Store.OSSE

Samply.Store.OSSE is an extension of Samply.Store, that is suited to the needs of the
OSSE project, that includes:

- the definition of the required entities and relationships
- the implementation of the access controller and different roles: Administrator,
    Developer, LocalAdministrator, Patient and Custom
  - The Administrator can change the application settings, but can not enter any medical data
  - The Developer role should be used for development only and can do anything
  - The Local Administrator role can assign create roles for a specific location, and
    can assign users to those roles, but he can not enter any medical data
  - The Patient role can not create new patients. This role can enter medical data for
    a specific patient only.
  - The Custom role can be created by the local administrator or administrator. This role doesn't have
    a set of predefined permissions, instead the local administrator or administrator
    must give the role permissions to do anything, e.g. "Create new medical Data"
- a REST interface, that offers a search functionality (Samply.Store.REST)
