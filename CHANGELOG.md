# Changelog Samply Store

## Version 1.4.0

- updated dependencies
- added option options to fetch adjacent resources
- added support for adding limits and offsets to resource queries
- added cache for configs
- added support for count queries

## Version 1.3.0

- added selections for `ResourceQuery`
- added convenience methods in the `AbstractResource`, e.g. use `getString` instead of `getProperty(...).getValue()`
- added the getjsonarray function
- added the usertype property
- added the SQL functions `GREATEST`, `LEAST`, `MIN` and `MAX`
- added support for `group by` and `having` in the SQL layer
- added support for the SQL Column Type `time`

## Version 1.2.0

- updated dependencies
- added some convenience methods for the query language
- fixed the generation of SQL for n-to-m relations

## Version 1.1.2

- the methods `saveResource` and `updateResource` now call the AccessController
  to remove unnecessary properties
