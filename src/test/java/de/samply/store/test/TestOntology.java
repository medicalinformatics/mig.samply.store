/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store.test;

public class TestOntology {

    public static final String ID = "id";

    public static class Type {
        public static final String ContainerType = "containerType";
        public static final String Form = "form";
        public static final String Transaction = "transaction";
        public static final String Location = "location";
        public static final String Query = "query";
        public static final String Visit = "visit";
        public static final String Permission = "permission";
        public static final String SampleContent = "sampleContent";
        public static final String Contact = "contact";
        public static final String Pseudonym = "pseudonym";
        public static final String Patient = "patient";
        public static final String Container = "container";
        public static final String Request = "request";
        public static final String Context = "context";
        public static final String Role = "role";
        public static final String Case = "case";
        public static final String User = "user";
        public static final String Sample = "sample";
    }


    public static class ContainerType {
        public static final String Label = "label";
        public static final String Size = "size";

        public static class ReadOnly {
            public static final String Containers = "containers";
        }
    }

    public static class Form {
        public static final String Name = "name";
        public static final String State = "state";
        public static final String Version = "version";
        public static final String Visit = "visit";

        public static class ReadOnly {
        }
    }

    public static class Location {
        public static final String Name = "name";
        public static final String Patients = "patients";

        public static class ReadOnly {
            public static final String Roles = "roles";
            public static final String Cases = "cases";
            public static final String Queries = "queries";
        }
    }

    public static class Query {
        public static final String Location = "location";

        public static class ReadOnly {
            public static final String Requests = "requests";
        }
    }

    public static class Visit {
        public static final String Name = "name";
        public static final String Case = "case";

        public static class ReadOnly {
            public static final String Forms = "forms";
        }
    }

    public static class Permission {
        public static final String Role = "role";

        public static class ReadOnly {
        }
    }

    public static class SampleContent {
        public static final String Amount = "amount";
        public static final String Unit = "unit";
        public static final String Container = "container";
        public static final String Sample = "sample";

        public static class ReadOnly {
        }
    }

    public static class Contact {
        public static final String User = "user";

        public static class ReadOnly {
        }
    }

    public static class Pseudonym {
        public static final String Value = "value";
        public static final String Context = "context";

        public static class ReadOnly {
        }
    }

    public static class Patient {
        public static final String Locations = "locations";

        public static class ReadOnly {
            public static final String Cases = "cases";
        }
    }

    public static class Container {
        public static final String ContainerType = "containerType";

        public static class ReadOnly {
            public static final String SampleContents = "sampleContents";
        }
    }

    public static class Request {
        public static final String Query = "query";

        public static class ReadOnly {
        }
    }

    public static class Context {
        public static final String Code = "code";

        public static class ReadOnly {
            public static final String Pseudonyms = "pseudonyms";
        }
    }

    public static class Role {
        public static final String Name = "name";
        public static final String Users = "users";
        public static final String Location = "location";

        public static class ReadOnly {
            public static final String Permissions = "permissions";
        }
    }

    public static class Case {
        public static final String Location = "location";
        public static final String Patient = "patient";

        public static class ReadOnly {
            public static final String Samples = "samples";
            public static final String Visits = "visits";
        }
    }

    public static class User {
        public static final String Username = "username";
        public static final String IsActivated = "isActivated";
        public static final String ActivationCode = "activationCode";
        public static final String FailCounter = "failCounter";
        public static final String Password = "password";
        public static final String Salt = "salt";
        public static final String Roles = "roles";

        public static class ReadOnly {
            public static final String Contacts = "contacts";
        }
    }

    public static class Sample {
        public static final String Case = "case";

        public static class ReadOnly {
            public static final String SampleContents = "sampleContents";
        }
    }
}
