/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store.test;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;

import javax.xml.bind.JAXBException;

import org.apache.logging.log4j.core.config.Configurator;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import de.samply.config.util.FileFinderUtil;
import de.samply.store.AccessController;
import de.samply.store.PSQLModel;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.exceptions.InvalidOperationException;
import de.samply.store.exceptions.TablesNotFoundException;
import de.samply.store.resources.Configuration;
import de.samply.store.test.subtests.BasicDatabaseTest;
import de.samply.store.test.subtests.ExceptionDatabaseTest;
import de.samply.store.test.subtests.QueryTest;
import de.samply.store.test.subtests.ResourceQueryTest;
import de.samply.store.test.subtests.ResourceTypesTest;
import de.samply.store.test.subtests.TypeDatabaseTest;
import de.samply.store.test.subtests.UserTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({ BasicDatabaseTest.class, ExceptionDatabaseTest.class,
        TypeDatabaseTest.class, ResourceTypesTest.class,
        UserTest.class, QueryTest.class,
        ResourceQueryTest.class})
public class StoreTestSuite {

    @BeforeClass
    public static void setUpClass() throws DatabaseException, JAXBException, SQLException, FileNotFoundException, UnsupportedEncodingException {
        Configurator.initialize("de.samply.store", "conf/log4j2.xml");

        ClassLoader loader = StoreTestSuite.class.getClassLoader();

        InputStream adminInputStream = loader.getResourceAsStream("admin.user.sql");

        if(adminInputStream == null) {
            throw new InvalidOperationException("File admin.user.sql does not exist!");
        }

        Configuration.parseResourceConfig(loader.getResourceAsStream("conf/tests.store.resources.xml"));
        PSQLModel<Void> model = newModel();

        try {
            model.testTables();
            assert(false);
        } catch(TablesNotFoundException e) {
            model.close();
            model = newModel();
            model.installDatabase();
            model.commit();
        }

//        model.installDatabase();
        model.executeStream(new InputStreamReader(adminInputStream, StandardCharsets.UTF_8.displayName()));
        model.commit();
        model.close();
    }

    public static PSQLModel<Void> newModel() throws DatabaseException, FileNotFoundException {
        return new PSQLModel<Void>(new AccessController<Void>(),
                FileFinderUtil.findFile("tests.store.database.xml").getAbsolutePath());
    }

    @AfterClass
    public static void tearDownClass() {
    }
}
