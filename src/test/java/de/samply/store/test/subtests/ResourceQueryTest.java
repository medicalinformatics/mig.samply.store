/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store.test.subtests;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import org.junit.Test;

import de.samply.store.Resource;
import de.samply.store.TimestampLiteral;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.query.AndCriteria;
import de.samply.store.query.Criteria;
import de.samply.store.query.OrCriteria;
import de.samply.store.query.ResourceQuery;
import de.samply.store.test.TestOntology;

public class ResourceQueryTest extends AbstractDatabaseTest {

    @Test
    public void genericTest() throws DatabaseException {
        Resource location = model.createResource(TestOntology.Type.Location);

        Random rand = new Random();
        String locationName = "test_group_" + rand.nextInt(100000);
        int patientAge = rand.nextInt(1238234);
        String icd10_location = "loc_" + rand.nextInt(2342342);
        long secs = new Date().getTime();

        location.setProperty(TestOntology.Location.Name, locationName);

        Resource patient = model.createResource(TestOntology.Type.Patient);
        patient.setProperty("age", patientAge);

        Resource Case = model.createResource(TestOntology.Type.Case);
        Case.setProperty(TestOntology.Case.Location, location);
        Case.setProperty(TestOntology.Case.Patient, patient);
        Case.setProperty("diagnosed", new TimestampLiteral(secs));

        Resource sample = model.createResource(TestOntology.Type.Sample);
        sample.setProperty(TestOntology.Sample.Case, Case);
        sample.setProperty("icd10_location", icd10_location);
        sample.setProperty("true", true);


        saveResources(location, patient, Case, sample);

        ResourceQuery query = new ResourceQuery(TestOntology.Type.Location);
        query.addRelation(TestOntology.Type.Location, TestOntology.Location.ReadOnly.Cases, TestOntology.Type.Case, false);
        query.addRelation(TestOntology.Type.Case, TestOntology.Case.Patient, TestOntology.Type.Patient);
        query.addRelation(TestOntology.Type.Sample, TestOntology.Sample.Case, TestOntology.Type.Case);
        query.add(Criteria.Between(TestOntology.Type.Patient, "age", patientAge - 1, patientAge + 1));
        query.add(Criteria.Equal(TestOntology.Type.Location, "name", locationName));
        query.add(Criteria.Equal(TestOntology.Type.Sample, "icd10_location", icd10_location));
        query.add(Criteria.Equal(TestOntology.Type.Case, "diagnosed", new TimestampLiteral(secs)));
        query.add(Criteria.Equal(TestOntology.Type.Sample, "true", true));

        ArrayList<Resource> results = model.getResources(query);

        assertTrue(results.size() == 1);
        assertTrue(results.get(0).asResource().getId() == location.getId());

        query = new ResourceQuery(TestOntology.Type.Patient);
        query.addRelation(TestOntology.Type.Case, TestOntology.Case.Patient, TestOntology.Type.Patient);
        query.addRelation(TestOntology.Type.Case, TestOntology.Location.ReadOnly.Cases, TestOntology.Type.Location, false);
        query.add(Criteria.Between(TestOntology.Type.Patient, "age", patientAge - 1, patientAge + 1));
        query.add(Criteria.Equal(TestOntology.Type.Location, "name", locationName));
        query.add(Criteria.Equal(TestOntology.Type.Sample, "icd10_location", icd10_location));
        query.add(Criteria.Equal(TestOntology.Type.Case, "diagnosed", new TimestampLiteral(secs)));

        results = model.getResources(query);

        assertTrue(results.size() == 1);
        assertTrue(results.get(0).asResource().getId() == patient.getId());

        query = new ResourceQuery(TestOntology.Type.Sample);
        query.add(Criteria.Between(TestOntology.Type.Patient, "age", patientAge - 1, patientAge + 1));
        query.add(Criteria.Equal(TestOntology.Type.Location, "name", locationName));
        query.add(Criteria.Equal(TestOntology.Type.Sample, "icd10_location", icd10_location));
        query.add(Criteria.Equal(TestOntology.Type.Case, "diagnosed", new TimestampLiteral(secs)));

        results = model.getResources(query);

        assertTrue(results.size() == 1);
        assertTrue(results.get(0).asResource().getId() == sample.getId());
    }

    @Test
    public void testSampleCountInGroup() throws DatabaseException {
        Resource location = model.createResource(TestOntology.Type.Location);

        Random rand = new Random();
        String locationName = "test_group_" + rand.nextInt(100000);

        location.setProperty(TestOntology.Location.Name, locationName);

        saveResources(location);

        int num = rand.nextInt(10);

        for(int i = 0; i < num; ++i) {
            Resource patient = model.createResource(TestOntology.Type.Patient);
            Resource Case = model.createResource(TestOntology.Type.Case);
            Resource sample = model.createResource(TestOntology.Type.Sample);
            Case.setProperty(TestOntology.Case.Patient, patient);
            Case.setProperty(TestOntology.Case.Location, location);
            sample.setProperty(TestOntology.Sample.Case, Case);

            saveResources(patient, Case, sample);
        }

        ResourceQuery query = new ResourceQuery(TestOntology.Type.Sample);
        query.add(Criteria.Equal(TestOntology.Type.Location, TestOntology.Location.Name, locationName));

        ArrayList<Resource> results = model.getResources(query);
        assertTrue(results.size() == num);

        query = new ResourceQuery(TestOntology.Type.Sample);
        query.add(Criteria.Equal(TestOntology.Type.Location, TestOntology.ID, location.getId()));

        results = model.getResources(query);
        assertTrue(results.size() == num);
    }

    @Test
    public void testFormsAndSample() throws DatabaseException {
        Resource location = model.createResource(TestOntology.Type.Location);
        Random r = new Random();
        String locationName = "test_group_" + r.nextInt(100000);
        String icd10 = "icd10_" + r.nextInt(43234112);
        int formState = r.nextInt(100000000);

        location.setProperty(TestOntology.Location.Name, locationName);

        saveResources(location);

        Resource patient = model.createResource(TestOntology.Type.Patient);
        Resource Case = model.createResource(TestOntology.Type.Case);
        Resource sample = model.createResource(TestOntology.Type.Sample);
        Resource form = model.createResource(TestOntology.Type.Form);
        Resource visit = model.createResource(TestOntology.Type.Visit);
        Case.setProperty(TestOntology.Case.Patient, patient);
        Case.setProperty(TestOntology.Case.Location, location);
        sample.setProperty(TestOntology.Sample.Case, Case);

        form.setProperty(TestOntology.Form.Visit, visit);
        form.setProperty(TestOntology.Form.State, "checked");
        form.setProperty(TestOntology.Form.Version, formState);
        form.setProperty(TestOntology.Form.Name, "another-not-important-name");

        visit.setProperty(TestOntology.Visit.Case, Case);
        visit.setProperty("timestamp", new TimestampLiteral(new Date().getTime()));
        visit.setProperty(TestOntology.Visit.Name, "not-important-visit-name");

        sample.setProperty("icd10", icd10);

        saveResources(patient, Case, sample, visit, form);

        ResourceQuery query = new ResourceQuery(TestOntology.Type.Sample);
        query.add(Criteria.Equal(TestOntology.Type.Form, TestOntology.Form.Version, formState));
        query.add(Criteria.Equal(TestOntology.Type.Location, TestOntology.Location.Name, locationName));
        query.add(Criteria.Equal(TestOntology.Type.Sample, "icd10", icd10));

        ArrayList<Resource> results = model.getResources(query);

        assertTrue(results.size() == 1);
        assertTrue(results.get(0).asResource().getId() == sample.getId());
    }

    @Test
    public void testSampleForUser() throws DatabaseException {
        Random r = new Random();
        String groupName = "test_group_" + r.nextInt(100000);
        String userName = "test_user_" + r.nextInt(100000);

        Resource location = model.createResource(TestOntology.Type.Location);
        location.setProperty(TestOntology.Location.Name, groupName);

        Resource user = model.createUser(userName, "not-important");

        Resource role = model.createResource(TestOntology.Type.Role);
        role.setProperty(TestOntology.Role.Name, "test_role_" + r.nextInt());
        role.setProperty(TestOntology.Role.Location, location);

        user.setProperty(TestOntology.User.Roles, role);

        Resource patient = model.createResource(TestOntology.Type.Patient);
        Resource Case = model.createResource(TestOntology.Type.Case);
        Resource sample = model.createResource(TestOntology.Type.Sample);

        Case.setProperty(TestOntology.Case.Patient, patient);
        Case.setProperty(TestOntology.Case.Location, location);
        sample.setProperty(TestOntology.Sample.Case, Case);

        saveResources(location, role, user, patient, Case, sample);

        ResourceQuery query = new ResourceQuery(TestOntology.Type.Sample);
        query.add(Criteria.Equal(TestOntology.Type.User, TestOntology.ID, user.getId()));

        ArrayList<Resource> results = model.getResources(query);
        assertTrue(results.size() == 1);
        assertTrue(results.get(0).getId() == sample.getId());
    }

    @Test
    public void testOr() throws DatabaseException {
        Random r = new Random();
        int age = r.nextInt(43533451);
        String name = "test_patient_" + r.nextInt(123434522);

        Resource patient = model.createResource(TestOntology.Type.Patient);
        patient.setProperty("age", age);
        patient.setProperty("name", name);

        saveResources(patient);

        ResourceQuery query = new ResourceQuery(TestOntology.Type.Patient);
        query.add(Criteria.Or(
                Criteria.Equal(TestOntology.Type.Patient, "age", age),
                Criteria.Equal(TestOntology.Type.Patient, "null-property", "never-defined-this_" + r.nextInt())));

        ArrayList<Resource> result = model.getResources(query);
        assertTrue(result.size() == 1);
        assertTrue(result.get(0).getId() == patient.getId());


        query = new ResourceQuery(TestOntology.Type.Patient);
        OrCriteria or = Criteria.Or();
        or.add(Criteria.Equal(TestOntology.Type.Patient, "name", name));
        or.add(Criteria.Equal(TestOntology.Type.Patient, "null-property", "never-defined-this_" + r.nextInt()));
        query.add(or);

        result = model.getResources(query);
        assertTrue(result.size() == 1);
        assertTrue(result.get(0).getId() == patient.getId());

        query = new ResourceQuery(TestOntology.Type.Patient);
        AndCriteria and = Criteria.And();
        and.add(Criteria.Equal(TestOntology.Type.Patient, "age", age));
        and.add(Criteria.Equal(TestOntology.Type.Patient, "name", name));
        query.add(and);

        result = model.getResources(query);
        assertTrue(result.size() == 1);
        assertTrue(result.get(0).getId() == patient.getId());
    }

    @Test
    public void testCriterias() throws DatabaseException {
        Resource patient = model.createResource(TestOntology.Type.Patient);

        Random r = new Random();
        int age = r.nextInt(123123131);

        patient.setProperty("age", age);

        saveResources(patient);

        ResourceQuery query = new ResourceQuery(TestOntology.Type.Patient);
        query.add(Criteria.NotEqual(TestOntology.Type.Patient, "age", age));

        ArrayList<Resource> results = model.getResources(query);

        for(Resource res : results) {
            if(res.getId() == patient.getId()) {
                assertTrue(1 == 0);
            }
        }

        query = new ResourceQuery(TestOntology.Type.Patient);
        query.add(Criteria.LessOrEqual(TestOntology.Type.Patient, "age", age));
        query.add(Criteria.GreaterOrEqual(TestOntology.Type.Patient, "age", age));

        results = model.getResources(query);
        assertTrue(results.size() == 1);
        assertTrue(results.get(0).getId() == patient.getId());

        query = new ResourceQuery(TestOntology.Type.Patient);
        query.add(Criteria.Less(TestOntology.Type.Patient, "age", age + 1));
        query.add(Criteria.Greater(TestOntology.Type.Patient, "age", age - 1));

        results = model.getResources(query);
        assertTrue(results.size() == 1);
        assertTrue(results.get(0).getId() == patient.getId());
    }

    @Test
    public void testIsNull() throws DatabaseException {
        Resource patient = model.createResource(TestOntology.Type.Patient);

        Random r = new Random();
        int age = r.nextInt(123123131);

        patient.setProperty("age", age);
        patient.setProperty("age2", r.nextFloat());

        saveResources(patient);

        ResourceQuery query = new ResourceQuery(TestOntology.Type.Patient);
        query.add(Criteria.Equal(TestOntology.Type.Patient, "age", age));
        query.add(Criteria.IsNotNull(TestOntology.Type.Patient, "age2"));

        ArrayList<Resource> results = model.getResources(query);

        assertTrue(results.size() == 1);
        assertTrue(results.get(0).getId() == patient.getId());


        query = new ResourceQuery(TestOntology.Type.Patient);
        query.add(Criteria.Equal(TestOntology.Type.Patient, "age", age));
        query.add(Criteria.IsNull(TestOntology.Type.Patient, "age1234"));

        results = model.getResources(query);

        assertTrue(results.size() == 1);
        assertTrue(results.get(0).getId() == patient.getId());
    }

}
