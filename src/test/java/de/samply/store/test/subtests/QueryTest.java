/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store.test.subtests;

import org.junit.Test;

import de.samply.store.exceptions.DatabaseException;

public class QueryTest extends AbstractDatabaseTest {

    @Test
    public void simpleQueryTest() throws DatabaseException {
//		Resource patient = model.createResource(TestOntology.Type.Patient);
//
//		Random rand = new Random();
//		String name = "Paulinator_" + rand.nextInt(10000000);
//		String year = "year " + rand.nextInt(10000000);
//
//		patient.addProperty("p-name", name);
//
//		Resource rcase = model.createResource(TestOntology.Type.Case);
//		rcase.setProperty(TestOntology.Case.Patient, patient);
//		rcase.addProperty("c-diagnosed", year);
//		rcase.setProperty(TestOntology.Case.Location, getLocation());
//
//		saveResources(patient, rcase);
//
//		View v = new View();
//		Query q = new Query();
//
//		Like e = new Like();
//		Attribute attr = new Attribute();
//		attr.setMdrKey("p-name");
//		attr.setValue("%Paulinator%");
//		e.setAttribute(attr);
//
//		Where where = new Where();
//		where.getAndOrEqOrLike().add(e);
//
//		attr = new Attribute();
//		attr.setMdrKey("c-diagnosed");
//		attr.setValue(year);
//
//		q.setWhere(where);
//
//		v.setQuery(q);
//
//		ViewFields vf = new ViewFields();
//		vf.getMdrKey().add("p-name");
//
//		v.getViewFields().add(vf);
//
//		model.executeQuery(v);
//
//		for(ArrayList<Object> row : result.getRows()) {
//			for(Object obj : row) {
//				System.out.println("Column: " + obj.toString());
//			}
//			System.out.println("--------");
//		}
//
//		assertTrue(result.getRows().size() == 1);
    }

}
