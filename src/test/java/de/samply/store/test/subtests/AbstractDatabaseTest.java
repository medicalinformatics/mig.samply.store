/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store.test.subtests;

import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.util.Random;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.samply.store.DatabaseModel;
import de.samply.store.Resource;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.test.StoreTestSuite;
import de.samply.store.test.TestOntology;

public class AbstractDatabaseTest {

    static DatabaseModel<Void> model;

    protected Random random = new Random();

    @BeforeClass
    public static void testSetup() throws DatabaseException, FileNotFoundException {
        model = StoreTestSuite.newModel();
    }

    @AfterClass
    public static void testCleanup() throws DatabaseException {
        model.close();
    }

    @Before
    @Test
    public void setup() throws DatabaseException {
        model.login("admin", "admin");
        assertTrue(model.getUserId() != 0);
    }

    @After
    public void tearDown() {
        model.logout();
    }

    protected void saveResources(Resource... resources) throws DatabaseException {
        model.beginTransaction();
        for(Resource r : resources) {
            model.saveOrUpdateResource(r);
        }
        model.commit();
    }

    protected Resource getLocation() throws DatabaseException {
        return model.getResource(TestOntology.Type.Location, 1);
    }

}
