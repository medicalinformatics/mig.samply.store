/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store.test.subtests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import de.samply.store.Resource;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.history.Transaction;
import de.samply.store.test.TestOntology;

public class UserTest extends AbstractDatabaseTest {

    @Test
    public void testAddUser() throws DatabaseException {
        String name = "paulinator_add_" + random.nextInt();
        String password = "nothing";
        Resource user = model.createUser(name, password);

        Resource contact = model.createResource(TestOntology.Type.Contact);
        contact.setProperty(TestOntology.Contact.User, user);
        contact.setProperty("location", "Damme");
        contact.setProperty("parentLocation", "Münster");

        saveResources(user, contact);

        model.logout();
        model.login(name, password);

        user = model.changeUserPassword(user, "nothing", "another password");
        assertTrue(user != null);
        saveResources(user);

        Resource newLocation = model.createResource(TestOntology.Type.Location);
        newLocation.setProperty(TestOntology.Location.Name, "another location");

        Resource newRole = model.createResource(TestOntology.Type.Role);
        newRole.setProperty(TestOntology.Role.Name, "another Role");

        newRole.setProperty(TestOntology.Role.Location, newLocation);

        user.addProperty(TestOntology.User.Roles, newRole);

        saveResources(newLocation, newRole, user);

        user = model.reloadResource(user);
        assertTrue(user.getProperties(TestOntology.User.Roles).size() == 1);
        assertTrue(user.getProperties(TestOntology.User.ReadOnly.Contacts).size() == 1);
        assertTrue(user.getProperty(TestOntology.User.Roles).asIdentifier().getId() == newRole.getId());
        assertTrue(user.getProperty(TestOntology.User.ReadOnly.Contacts).asIdentifier().getId() == contact.getId());

        List<Transaction> transactions = model.getTransactions(user);
        assertTrue(transactions.size() == 3);

        Resource r = model.getResource(user, transactions.get(2));

        assertFalse(r.getProperty(TestOntology.User.Password).getValue()
                .equals(user.getProperty(TestOntology.User.Password).getValue()));

        Resource role = model.createResource(TestOntology.Type.Role);
        role.setProperty(TestOntology.Role.Name, "test-role");
        role.setProperty(TestOntology.Role.Location, newLocation);

        user.addProperty(TestOntology.User.Roles, role);

        saveResources(role, user);

        role = model.createResource(TestOntology.Type.Role);
        role.setProperty(TestOntology.Role.Name, "another test-group");
        role.setProperty(TestOntology.Role.Location, newLocation);
        user.addProperty(TestOntology.User.Roles, role);

        saveResources(role, user);

        transactions = model.getTransactions(user);
        assertTrue(transactions.size() == 5);

        r = model.getResource(user, transactions.get(2));
        assertTrue(r.getProperties(TestOntology.User.Roles).size() == 1);

        r = model.getResource(user, transactions.get(1));
        assertTrue(r.getProperties(TestOntology.User.Roles).size() == 2);

        r = model.getResource(user, transactions.get(0));
        assertTrue(r.getProperties(TestOntology.User.Roles).size() == 3);
    }

    @Test
    public void registerNewUserTest() throws DatabaseException {
        String username = "paulinator_" + random.nextInt();
        Resource user = model.createUser(username, "");

        saveResources(user);
        model.logout();
        model.activateUser("another-password", user.getProperty(TestOntology.User.ActivationCode).getValue());
        model.login(username, "another-password");
    }

}
