/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store.test.subtests;

import org.junit.Test;

import de.samply.store.Resource;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.exceptions.FailCounterException;
import de.samply.store.exceptions.LoginFailedException;
import de.samply.store.exceptions.NoLoginException;
import de.samply.store.exceptions.NoTransactionException;
import de.samply.store.exceptions.PropertyNotDefined;
import de.samply.store.exceptions.TransactionException;
import de.samply.store.exceptions.UserNotActivatedException;
import de.samply.store.test.TestOntology;

public class ExceptionDatabaseTest extends AbstractDatabaseTest {

    @Test(expected=PropertyNotDefined.class)
    public void failPropertyNotDefined() throws DatabaseException {
        Resource pCase = model.createResource(TestOntology.Type.Case);
        saveResources(pCase);
    }

    @Test(expected=TransactionException.class)
    public void failTransaction() throws DatabaseException {
        model.beginTransaction();
        model.beginTransaction();
    }

    @Test(expected=LoginFailedException.class)
    public void failLogin() throws DatabaseException {
        model.logout();
        model.login("test", "fail");
    }

    @Test(expected=NoLoginException.class)
    public void failNoLogin1() throws DatabaseException {
        model.logout();
        model.getResource(TestOntology.Type.Patient, 1);
    }

    @Test(expected=NoLoginException.class)
    public void failNoLogin2() throws DatabaseException {
        model.logout();
        model.getResources(TestOntology.Type.Patient);
    }

    @Test(expected=NoLoginException.class)
    public void failNoLogin3() throws DatabaseException {
        model.logout();
        Resource patient = model.createResource(TestOntology.Type.Patient);
        saveResources(patient);
    }

    @Test(expected=NoTransactionException.class)
    public void failNoTransaction() throws DatabaseException {
        Resource patient = model.createResource(TestOntology.Type.Patient);
        model.saveOrUpdateResource(patient);
    }

    @Test(expected=UserNotActivatedException.class)
    public void failNotActivated() throws DatabaseException {
        String username = "fail_user_" + random.nextInt();
        saveResources(model.createUser(username, ""));
        model.logout();
        model.login(username, "");
    }

    @Test(expected=FailCounterException.class)
    public void failCounter() throws DatabaseException {
        String username = "fail_user_" + random.nextInt();
        saveResources(model.createUser(username, "password"));
        model.logout();
        for(int i = 0; i < 10; ++i) {
            try {
                model.login(username, "wrongPassword");
            } catch(DatabaseException e) {
            }
        }
        model.login(username, "password");
    }

    @Test(expected=DatabaseException.class)
    public void rollbackTest() throws DatabaseException {
        Resource patient = model.createResource(TestOntology.Type.Patient);

        patient.setProperty("name", "paulinator");

        model.beginTransaction();
        model.saveOrUpdateResource(patient);
        model.rollback();

        model.reloadResource(patient);
    }
}
