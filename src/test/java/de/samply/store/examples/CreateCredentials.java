/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store.examples;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Scanner;

import de.samply.store.security.PasswordHash;

/**
 * Just a small "script" that creates SQL-Statements that create
 * a new user after your input.
 *
 */
public class CreateCredentials {

    public static void main(String[] args) {
        Scanner inp = new Scanner(System.in, StandardCharsets.UTF_8.displayName());
        ArrayList<String> strings = new ArrayList<String>();

        System.out.println("Username: ");
        String username = inp.nextLine();

        System.out.println("Password: ");
        String password = inp.nextLine();
        String salt = PasswordHash.generateSalt();

        String hashedPassword = PasswordHash.hashPassword(password, salt);

        System.out.println("Username: " + username);
        System.out.println("Password: " + password);
        System.out.println("Salt: " + salt);

        System.out.println("Hashed Password: " + hashedPassword);

        System.out.println("");



        System.out.println("Create new role and location? [y/n] ");
        String i = inp.nextLine();

        if(i.equals("y")) {
            System.out.println("Location: ");
            String location = inp.nextLine();

            strings.add("INSERT INTO locations (data, name, transaction_id) VALUES ('{}', '"
                    + location + "', 1);");

            System.out.println("Rolename: ");
            String rolename = inp.nextLine();

            System.out.println("roleType: [USER, ADMINISTRATOR, LOCAL_ADMINISTRATOR] ");
            String roleType = inp.nextLine();

            strings.add("INSERT INTO \"roles\" (data, name, location_id, transaction_id) VALUES "
                    + "('{\"roleType\":\"" + roleType + "\"}', '" + rolename + "', currval('locations_id_seq'), "
                    + "1);");

        }

        strings.add("INSERT INTO \"users\" (data, username, password, salt, "
                + "\"isActivated\", \"activationCode\", \"failCounter\", transaction_id) VALUES ("
                + "'{}', '" + username + "', '" + hashedPassword + "', '" + salt + "', 1, '', 0, 1"
                        + ");");

        strings.add("INSERT INTO \"users_roles\" (user_id, role_id, transaction_id) VALUES ("
                + "currval('users_id_seq'), currval('roles_id_seq'), 1);");

        System.out.println("");

        for(String s : strings) {
            System.out.println(s);
        }

        inp.close();
    }

}
