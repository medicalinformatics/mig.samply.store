CREATE OR REPLACE FUNCTION resourceID(input text) RETURNS INTEGER AS
$BODY$
    SELECT split_part(input, ':', 2)::INTEGER
$BODY$ LANGUAGE SQL STRICT;

CREATE OR REPLACE FUNCTION resourceType(input text) RETURNS TEXT AS
$BODY$
    SELECT split_part(input, ':', 1)::TEXT
$BODY$ LANGUAGE SQL STRICT;


CREATE OR REPLACE FUNCTION getjsonarray(value json)
 RETURNS json AS
$BODY$
BEGIN
-- IF json_typeof(value) = 'array' THEN   // For Postgresql 9.4
   IF substring(trim(leading from (value::text)) from 1 for 1) = '[' THEN
       RETURN value;
   ELSE
       RETURN array_to_json(array_agg(value));
   END IF;
END;
$BODY$
LANGUAGE plpgsql;





CREATE OR REPLACE FUNCTION asbigint(value text)
  RETURNS bigint AS
$BODY$
BEGIN
    RETURN value::BIGINT;
EXCEPTION WHEN others then
    RETURN NULL;
END;

$BODY$
  LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION asboolean(value text)
  RETURNS boolean AS
$BODY$
BEGIN
    RETURN value::BOOLEAN;
EXCEPTION WHEN others then
    RETURN NULL;
END;

$BODY$
  LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION asdecimal(value text)
  RETURNS numeric AS
$BODY$
BEGIN
    RETURN value::DECIMAL;
EXCEPTION WHEN others then
    RETURN NULL;
END;

$BODY$
  LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION astimestamp(value text)
  RETURNS timestamp without time zone AS
$BODY$
BEGIN
    RETURN value::timestamp;
EXCEPTION WHEN others then
    RETURN NULL;
END;

$BODY$
  LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION astime(value text)
  RETURNS time AS
$BODY$
BEGIN
    RETURN value::time;
EXCEPTION WHEN others then
    RETURN NULL;
END;

$BODY$
  LANGUAGE plpgsql;
