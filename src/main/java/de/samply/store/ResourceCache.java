/*
 * Copyright (C) 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store;

import java.util.HashMap;

import de.samply.store.exceptions.DatabaseException;

/**
 * A simple Resource Cache. Caches Resources identified by their resource identifier.
 */
public class ResourceCache {

    private HashMap<String, Resource> map = new HashMap<>();

    private HashMap<String, JSONResource> configCache = new HashMap<>();

    private final PSQLModel<?> model;

    public ResourceCache(PSQLModel<?> model) {
        this.model = model;
    }

    public Resource handle(String type, int id) throws DatabaseException {
        if(map.containsKey(type + ":" + id)) {
            return map.get(type + ":" + id);
        } else {
            Resource resource = model.getResource(type, id, false);
            map.put(type + ":" + id, resource);
            return resource;
        }
    }

    public JSONResource getConfig(String configName) {
        if(configCache.containsKey(configName)) {
            return configCache.get(configName);
        } else {
            return null;
        }
    }

    public void clear() {
        map.clear();
        configCache.clear();
    }

    public void add(String resourceType, int id, Resource resource) {
        if(!map.containsKey(resourceType + ":" + id)) {
            map.put(resourceType + ":" + id, resource);
        }
    }

    public void add(String configName, JSONResource config) {
        if(!configCache.containsKey(configName)) {
            configCache.put(configName, config);
        }
    }
}
