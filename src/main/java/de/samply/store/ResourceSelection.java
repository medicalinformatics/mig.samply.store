/*
 * Copyright (C) 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store;

import de.samply.store.sql.SQLColumn;

import java.util.HashMap;

/**
 * Created by paul on 11.07.16.
 */
public class ResourceSelection {

    public final SQLColumn idColumn;

    public final SQLColumn dataColumn;

    public final SQLColumn transactionIdColumn;

    public final boolean primary;

    private HashMap<SQLColumn, String> selections = new HashMap<>();

    private HashMap<TableDefinition.Join, SQLColumn> joinColumns = new HashMap<>();

    public ResourceSelection(SQLColumn idColumn, SQLColumn dataColumn, SQLColumn transactionIdColumn, boolean primary) {
        this.idColumn = idColumn;
        this.dataColumn = dataColumn;
        this.transactionIdColumn = transactionIdColumn;
        this.primary = primary;
    }

    public void add(SQLColumn col) {
        selections.put(col, null);
    }

    public void add(String property, SQLColumn col) {
        selections.put(col, property);
    }

    public HashMap<SQLColumn, String> getPropertySelections() {
        return selections;
    }

    public HashMap<TableDefinition.Join, SQLColumn> getJoinColumns() {
        return joinColumns;
    }

    public void setJoinColumns(HashMap<TableDefinition.Join, SQLColumn> joinColumns) {
        this.joinColumns = joinColumns;
    }
}
