/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store.sql.functions;

import de.samply.store.sql.Binder;
import de.samply.store.sql.SQLValue;
import de.samply.string.util.StringUtil;
import de.samply.string.util.StringUtil.Builder;

/**
 *
 */
public class Least extends SQLFunction {

    private SQLValue[] values;

    public Least(SQLValue... values) {
        this.values = values;
    }

    /* (non-Javadoc)
     * @see de.samply.store.sql.SQLValue#construct(de.samply.store.sql.Binder)
     */
    @Override
    public String construct(final Binder b) {
        return "LEAST(" + StringUtil.join(values, ", ", new Builder<SQLValue>() {
            @Override
            public String build(SQLValue o) {
                return o.construct(b);
            }
        }) + ")";
    }

}
