/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store.sql;

import java.util.HashMap;

import de.samply.store.DatabaseConstants;

/**
 * A SQL table that can be access with an alias.
 *
 */
public class SQLTable {

    /**
     * The table name
     */
    protected String table;

    /**
     * The SQL alias for the SELECT statement.
     */
    protected String alias;

    /**
     * The currently available columns.
     */
    private HashMap<String, SQLColumn> columns = new HashMap<>();

    /**
     * The current alias index.
     */
    private int aliasIndex = 0;

    public SQLColumn getConstant(AbstractSQLValue<?> constant, SQLColumnType type) {
        return new SQLColumn(this, constant, type, newColumnAlias());
    }

    public SQLColumn getColumn(String column) {
        if(columns.containsKey(column)) {
            return columns.get(column);
        } else {
            return null;
        }
    }

    public SQLColumn getColumn(String column, SQLColumnType type) {
        if(columns.containsKey(column)) {
            return columns.get(column);
        } else {
            SQLColumn col = new SQLColumn(this, column, newColumnAlias(), type);
            columns.put(column, col);
            return col;
        }
    }

    public SQLColumn getColumn(String column, SQLColumnType type, String jsonProperty) {
        if(columns.containsKey(column)) {
            // If the column is from type JSON_OBJ, we need to return a new SQLColumn
            SQLColumn col = columns.get(column);
            if(col.type == SQLColumnType.JSON_OBJ || column.equals(DatabaseConstants.dataColumn)) {
                return new SQLColumn(col.table, column, newColumnAlias(), type, jsonProperty);
            } else if(type == SQLColumnType.RESOURCE && col.type == SQLColumnType.INTEGER) {
                return new SQLColumn(col.table, column, col.alias, type, jsonProperty);
            }
            return col;
        } else {
            SQLColumn col = new SQLColumn(this, column, newColumnAlias(), type, jsonProperty);
            columns.put(column, col);
            return col;
        }
    }

    /**
     * Returns a SQLColumn that accesses an array from the JSON OBJ.
     */
    public SQLColumn getJsonArrayColumn(String column, String property) {
        return new SQLJsonArrayColumn(this, getColumn(column, SQLColumnType.JSON_OBJ, property));
    }

    /**
     * Returns the name of this table.
     */
    public String getTable() {
        return table;
    }

    /**
     * Returns the quoted table name
     * @param b
     * @return
     */
    public String getTable(Binder b) {
        return "\"" + table + "\"";
    }

    /**
     * Sets the table.
     * @param table
     */
    public void setTable(String table) {
        this.table = table;
    }

    /**
     * Returns the SQL alias
     * @return
     */
    public String getAlias() {
        return alias;
    }

    /**
     * Sets the SQL alias.
     * @param alias
     */
    public void setAlias(String alias) {
        this.alias = alias;
    }

    /**
     * Returns the next free alias that can be used for columns.
     * @return
     */
    protected String newColumnAlias() {
        return getAlias() + "_c_" + (aliasIndex++);
    }
}
