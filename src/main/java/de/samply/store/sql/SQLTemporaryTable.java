/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store.sql;

/**
 * Represents a temporary SQL table, e.g. (SELECT abx FROM ACD ...).
 *
 */
public class SQLTemporaryTable extends SQLTable {

    /**
     * This class uses the standard SQLQuery class to construct a
     * valid SQL SELECT statement. Then it wraps the constructed
     * SELECT statement into parentheses.
     */
    private final SQLQuery sql;

    /**
     * If true, this temporary table will generate a query as follows:
     *
     * <pre>
     * (WITH x AS (*QUERY*)
     *
     * SELECT x.*, row_number() OVER () as rnum FROM x)
     * </pre>
     */
    private boolean number = false;

    /**
     * Constructs a new temporary table that can be used in the
     * base SQL query.
     * @param sql
     * @param base
     */
    public SQLTemporaryTable(SQLQuery sql, SQLQuery base) {
        this(sql, base, false);
    }

    /**
     *
     */
    public SQLTemporaryTable(SQLQuery sql, SQLQuery base, boolean number) {
        this.sql = sql;
        this.alias = base.newTableAlias();
        this.number = number;
    }

    @Override
    public String getTable(Binder b) {
        if(number) {
            return "(WITH x AS ( " + sql.construct(b) + ") SELECT x.*, row_number() OVER () as rnum FROM x)";
        } else {
            return "(" + sql.construct(b) + ")";
        }
    }

    /**
     * Returns the column that can be accessed from <u>outside</u> the temporary table.
     * @param column
     * @return
     */
    public SQLColumn getOutsideColumn(SQLColumn column) {
        return new SQLColumn(this, column.alias, newColumnAlias(), column.type);
    }

    /**
     * Returns the column that can be accessed from <u>outside</u> the temporary table.
     * @param column
     * @return
     */
    public SQLColumn getOutsideColumn(SQLColumn column, SQLColumnType type, String jsonProperty) {
        return new SQLColumn(this, column.alias, newColumnAlias(), type, jsonProperty);
    }

    /**
     * @return the sql
     */
    public SQLQuery getSql() {
        return sql;
    }

}
