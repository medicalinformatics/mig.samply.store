/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store.sql;

import de.samply.store.sql.clauses.AndClause;
import de.samply.store.sql.clauses.Clause;
import de.samply.store.sql.clauses.EqualClause;
import de.samply.store.sql.clauses.GreaterClause;
import de.samply.store.sql.clauses.GreaterOrEqualClause;
import de.samply.store.sql.clauses.InClause;
import de.samply.store.sql.clauses.IsNotNullClause;
import de.samply.store.sql.clauses.IsNullClause;
import de.samply.store.sql.clauses.LessClause;
import de.samply.store.sql.clauses.LessOrEqualClause;
import de.samply.store.sql.clauses.LikeClause;
import de.samply.store.sql.clauses.NotEqualClause;
import de.samply.store.sql.clauses.OrClause;
import de.samply.store.sql.clauses.SimilarToClause;

/**
 * Just a helper class for Clauses
 *
 */
public class Clauses {

    public static Clause SimilarTo(SQLValue l, SQLValue r) {
        return new SimilarToClause(l, r);
    }

    public static Clause And(Clause l, Clause r) {
        return new AndClause(l, r);
    }

    public static Clause Or(Clause l, Clause r) {
        return new OrClause(l, r);
    }

    public static Clause Like(SQLValue l, SQLValue r) {
        return new LikeClause(l, r);
    }

    public static Clause Equal(SQLValue l, SQLValue r) {
        return new EqualClause(l, r);
    }

    public static Clause NotEqual(SQLValue l, SQLValue r) {
        return new NotEqualClause(l, r);
    }

    public static Clause Greater(SQLValue l, SQLValue r) {
        return new GreaterClause(l, r);
    }

    public static Clause Less(SQLValue l, SQLValue r) {
        return new LessClause(l, r);
    }

    public static Clause GreaterOrEqual(SQLValue l, SQLValue r) {
        return new GreaterOrEqualClause(l, r);
    }

    public static Clause LessOrEqual(SQLValue l, SQLValue r) {
        return new LessOrEqualClause(l, r);
    }

    public static Clause In(SQLColumn column, SQLQuery sqlQuery) {
        return new InClause(column, sqlQuery);
    }

    public static Clause Between(SQLValue l, SQLRangeValue to) {
        return new BetweenClause(l, to);
    }

    public static Clause IsNull(SQLColumn column) {
        return new IsNullClause(column);
    }

    public static Clause IsNotNull(SQLColumn column) {
        return new IsNotNullClause(column);
    }

}
