/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.samply.store.sql.Order.Type;
import de.samply.store.sql.clauses.Clause;
import de.samply.string.util.StringUtil;
import de.samply.string.util.StringUtil.Builder;

/**
 * A class that helps you creating an SQL query
 *
 */
public class SQLQuery {

    private static final Logger logger = LogManager.getLogger(SQLQuery.class);

    /**
     * The main SQL table. This table is used in the "FROM" part of the SELECT statement.
     */
    private SQLTable mainTable = null;

    /**
     * The current free alias index.
     */
    private int aliasIndex = 0;

    /**
     * The list of all currently used SQL tables.
     */
    private List<SQLTable> tables = new ArrayList<>();

    /**
     * The current selection in a hash map with the properties as values.
     */
    private HashMap<SQLColumn, String> selection = new HashMap<>();

    /**
     * The list of joins.
     */
    private List<SQLJoin> joins = new ArrayList<>();

    /**
     * The list of clauses
     */
    private List<Clause> clauses = new ArrayList<>();

    /**
     * The list of orders that will be used for this query.
     */
    private List<Order> orders = new ArrayList<Order>();

    /**
     * The list of group bys.
     */
    private List<SQLColumn> groupBy = new ArrayList<SQLColumn>();

    /**
     * The list of clauses for the having part.
     */
    private List<Clause> having = new ArrayList<>();

    /**
     * The SQL limit of this SELECT query.
     */
    private int limit = 0;

    /**
     * The SQL offset of this SELECT query.
     */
    private int offset = 0;

    /**
     * Creates a new SQL query for the given table.
     * @param table
     */
    public SQLQuery(String table) {
        mainTable = getTable(table);
    }

    /**
     * Returns the main SQL table.
     * @return
     */
    public SQLTable getMainTable() {
        return mainTable;
    }

    /**
     * Adds the specified column to the "SELECT" statement.
     * @param col
     */
    public void addSelection(SQLColumn col) {
        selection.put(col, null);
    }

    /**
     * Adds the specified column to the "SELECT" statement. The value
     * will be used as a property in the resource later on.
     * @param property
     * @param col
     */
    public void addSelection(String property, SQLColumn col) {
        selection.put(col, property);
    }

    /**
     * Returns a hashmap of all selected columns.
     * @return
     */
    public HashMap<SQLColumn, String> getSelection() {
        return selection;
    }

    /**
     * Adds an order to the select statement.
     * @param order
     */
    public void addOrder(Order order) {
        orders.add(order);
    }

    /**
     * Returns the SQL table for the given table name.
     * @param table
     * @return
     */
    public SQLTable getTable(String table) {
        for(SQLTable sqlTable : tables) {
            if(sqlTable.table.equals(table)) {
                return sqlTable;
            }
        }
        return addTable(table);
    }

    /**
     * Adds the given table to the list of tables. This method does not check if
     * a table has been added already. Useful for joining a table twice.
     * @param table
     * @return
     */
    public SQLTable addTable(String table) {
        SQLTable t = new SQLTable();
        t.setTable(table);
        t.setAlias(newTableAlias());

        tables.add(t);

        return t;
    }

    /**
     * Returns a new alias for tables.
     * @return
     */
    public String newTableAlias() {
        return "t_" + (aliasIndex++);
    }

    /**
     * Creates a PreparedStatement from this SQLQuery using the specified SQL connection.
     * @param c
     * @return
     * @throws SQLException
     */
    public PreparedStatement prepare(Connection c) throws SQLException {
        return prepare(c, false);
    }

    /**
     * Creates a PreparedStatement from this SQLQuery using the specified SQL connection.
     * @param c
     * @param countOnly set to true if only select count(*) shall be performed
     * @return
     * @throws SQLException
     */
    public PreparedStatement prepare(Connection c, boolean countOnly) throws SQLException {

        Binder b = new Binder();
        String sql;
        if (countOnly) {
            sql = constructCount(b);
        } else {
            sql = construct(b);
        }
        logger.debug("SQL: " + sql);
        PreparedStatement st = c.prepareStatement(sql);
        b.bind(st);
        return st;
    }

    /**
     * Creates the SQL statement and returns it.
     * @return
     */
    public String construct() {
        return construct(new Binder());
    }

    /**
     * Creates the SQL statement with the specified binder (may be null), and returns it.
     * @return
     */
    public String construct(final Binder b) {
        if(b == null) {
            throw new UnsupportedOperationException("Binder is null!");
        }

        StringBuilder builder = new StringBuilder("SELECT ");

        List<String> parts = new ArrayList<String>();
        // The selections
        for(SQLColumn col : selection.keySet()) {
            parts.add(col.access(b) + " AS " + col.alias);
        }

        builder.append(StringUtil.join(parts, ", "));

        builder.append(" FROM ").append(mainTable.getTable(b))
                .append(" AS ").append(mainTable.getAlias());

        for(SQLJoin j : joins) {
            builder.append(" LEFT JOIN ").append(j.table.getTable(b)).append(" AS ").append(j.table.getAlias())
                .append(" ON ").append(j.left.construct(b))
                .append(" = ").append(j.right.construct(b));
        }

        if(this.clauses.size() != 0) {
            builder.append(" WHERE ");
            parts = new ArrayList<String>();
            for(Clause c : clauses) {
                parts.add(c.construct(b));
            }
            builder.append(StringUtil.join(parts, " AND "));
        }

        if(groupBy.size() > 0) {
            builder.append(" GROUP BY ");
            builder.append(StringUtil.join(groupBy, ", ", new Builder<SQLColumn>() {
                @Override
                public String build(SQLColumn o) {
                    return o.construct(b);
                }
            }));
        }

        if(having.size() > 0) {
            builder.append(" HAVING ");
            builder.append(StringUtil.join(having, ", ", new Builder<Clause>() {
                @Override
                public String build(Clause o) {
                    return o.construct(b);
                }
            }));
        }

        if(orders.size() > 0) {
            builder.append(" ORDER BY ");
            for(Order o : orders) {
                builder.append(o.column.construct(b))
                    .append(o.type == Type.ASCENDING ? " ASC" : " DESC");
            }
        }

        if(limit > 0) {
            builder.append(" LIMIT ").append(limit);
        }

        if(offset > 0) {
            builder.append(" OFFSET ").append(offset);
        }

        return builder.toString();
    }

    public String constructCount() {
        return constructCount(new Binder());
    }

    /**
     * Creates the SQL statement with the specified binder (may be null), and returns it.
     * @return
     */
    public String constructCount(final Binder b) {
        if(b == null) {
            throw new UnsupportedOperationException("Binder is null!");
        }

        StringBuilder builder = new StringBuilder("SELECT COUNT(*) ");

        List<String> parts = new ArrayList<String>();

        builder.append(" FROM ").append(mainTable.getTable(b))
                .append(" AS ").append(mainTable.getAlias());

        for(SQLJoin j : joins) {
            builder.append(" LEFT JOIN ").append(j.table.getTable(b)).append(" AS ").append(j.table.getAlias())
                    .append(" ON ").append(j.left.construct(b))
                    .append(" = ").append(j.right.construct(b));
        }

        if(this.clauses.size() != 0) {
            builder.append(" WHERE ");
            parts = new ArrayList<String>();
            for(Clause c : clauses) {
                parts.add(c.construct(b));
            }
            builder.append(StringUtil.join(parts, " AND "));
        }

        return builder.toString();
    }

    /**
     * Adds a join to the select statement.
     * @param table
     * @param left
     * @param right
     */
    public void addJoin(SQLTable table, SQLValue left, SQLValue right) {
        joins.add(new SQLJoin(table, left, right));
    }

    /**
     * Adds a WHERE clause to the select statement.
     * @param clause
     */
    public void addClause(Clause clause) {
        this.clauses.add(clause);
    }

    /**
     * Adds a "HAVING" clause
     * @param clause
     */
    public void addHaving(Clause clause) {
        this.having.add(clause);
    }

    public void addGroupBy(SQLColumn column) {
        this.groupBy.add(column);
    }

    /**
     * Returns the current limit.
     * @return
     */
    public int getLimit() {
        return this.limit;
    }

    /**
     * Sets the limit.
     * @param limit
     */
    public void setLimit(int limit) {
        this.limit = limit;
    }

    /**
     * Returns the offset.
     * @return
     */
    public int getOffset() {
        return offset;
    }

    /**
     * Sets the offset.
     * @param offset
     */
    public void setOffset(int offset) {
        this.offset = offset;
    }

    /**
     * Searches the current table map for the specified table and returns it.
     * Returns null if the table map does not contain the specified table.
     * @param table
     * @return
     */
    public SQLTable findTable(String table) {
        for(SQLTable sqlTable : tables) {
            if(sqlTable.table.equals(table)) {
                return sqlTable;
            }
        }
        return null;
    }

    /**
     * Describes an actual SQL join.
     *
     */
    private static class SQLJoin {
        public final SQLTable table;
        public final SQLValue left;
        public final SQLValue right;

        public SQLJoin(SQLTable table, SQLValue left, SQLValue right) {
            this.table = table;
            this.left = left;
            this.right = right;
        }
    }
}
