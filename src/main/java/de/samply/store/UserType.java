/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store;

/**
 * The role type. Depending on the role type the appropriate role handler is loaded.
 *
 * Administrators can change basic registry settings like forms,
 * URLs (Auth, MDR or Form Repository), etc. Local administrators can assign new
 * users to roles from their own location. For those two role types the permission
 * object are <u>ignored</u>. Neither the administrator nor the local administrator
 * can access medical data (no patients, no cases, etc.). Only for custom roles
 * the permission objects are read and handled. Therefore: if the custom role has
 * permission objects that grant access to patients, the role can read those patients.
 *
 * The developer role can be used for development. The developer role is allowed to
 * do everything.
 *
 */
public enum UserType {DEFAULT}
