/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store.query;

import de.samply.store.sql.SQLColumnType;

/**
 *
 */
public class ArrayValueColumn {

    /**
     * The type of the resource for this column.
     */
    private final String type;

    /**
     * The URN of the record
     */
    private final String recordUrn;

    /**
     * The name.
     */
    private final String name;

    /**
     * The SQL column type.
     */
    private final SQLColumnType sqlColumnType;

    /**
     * The property that will be accessed.
     */
    private final String property;

    public ArrayValueColumn(String type, String recordUrn, String name, SQLColumnType sqlColumnType, String property) {
        this.type = type;
        this.recordUrn = recordUrn;
        this.name = name;
        this.sqlColumnType = sqlColumnType;
        this.property = property;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @return the recordUrn
     */
    public String getRecordUrn() {
        return recordUrn;
    }

    /**
     * @return the sqlColumnType
     */
    public SQLColumnType getSqlColumnType() {
        return sqlColumnType;
    }

    /**
     * @return the property
     */
    public String getProperty() {
        return property;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

}
