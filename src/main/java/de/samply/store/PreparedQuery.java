/*
 * Copyright (C) 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store;

import java.util.HashMap;

import de.samply.store.sql.SQLColumn;
import de.samply.store.sql.SQLQuery;

/**
 * A prepared SQL query.
 *
 * @param <T>
 */
public class PreparedQuery<T> {

    /**
     * The SQL query
     */
    final SQLQuery sqlQuery;

    /**
     * The joins and their columns in a hashmap
     */
    final HashMap<TableDefinition.Join, SQLColumn> joinColumns;

    /**
     * The data column
     */
    final SQLColumn dataColumn;

    /**
     * The id column
     */
    final SQLColumn idColumn;

    /**
     * The transaction column
     */
    final SQLColumn transactionColumn;

    /**
     * The access controller context
     */
    final AccessContext<T> context;

    /**
     * The resource type
     */
    final String resourceType;

    /**
     * The table definition
     */
    final TableDefinition tableDefinition;

    /**
     * The hashmap for the selections.
     */
    final HashMap<String, ResourceSelection> fetchedSelections;

    public PreparedQuery(SQLQuery sqlQuery,
                         HashMap<TableDefinition.Join, SQLColumn> joinColumns, HashMap<String, ResourceSelection> fetchedSelections, SQLColumn dataColumn,
                         SQLColumn idColumn, SQLColumn transactionColumn,
                         AccessContext<T> context, String resourceType,
                         TableDefinition tableDefinition) {
        this.sqlQuery = sqlQuery;
        this.joinColumns = joinColumns;
        this.fetchedSelections = fetchedSelections;
        this.dataColumn = dataColumn;
        this.idColumn = idColumn;
        this.transactionColumn = transactionColumn;
        this.context = context;
        this.resourceType = resourceType;
        this.tableDefinition = tableDefinition;
    }

    public HashMap<String, ResourceSelection> getFetchedSelections() {
        return fetchedSelections;
    }
}
