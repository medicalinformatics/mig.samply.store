/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.util.Map.Entry;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import de.samply.common.config.Postgresql;
import de.samply.common.config.Store;
import de.samply.config.util.JAXBUtil;
import de.samply.store.TableDefinition.Join;
import de.samply.store.TableDefinition.Join.JoinType;
import de.samply.store.TableDefinition.TableColumn;
import de.samply.store.exceptions.*;
import de.samply.store.history.Transaction;
import de.samply.store.query.Criteria;
import de.samply.store.query.ResourceQuery;
import de.samply.store.resources.Configuration;
import de.samply.store.security.PasswordHash;
import de.samply.store.sql.*;

/**
 * DatabaseModel implementation for PostgreSQL 9.3/9.4
 *
 * @param <T> The context for the AccessController
 */
public class PSQLModel<T> extends DatabaseModel<T> {

    protected static final Logger logger = LogManager.getLogger(PSQLModel.class);

    private static JAXBContext context = null;

    /**
     * Returns a JAXBContext. Creates one if it does not exist yet.
     * @return
     * @throws JAXBException
     */
    private static JAXBContext getJAXBContext() throws JAXBException {
        if(context == null) {
            context = JAXBContext.newInstance(Store.class, Postgresql.class);
        }

        return context;
    }

    /**
     * The current backend configuration
     */
    protected Store config;

    /**
     * The SQL Connection
     */
    protected Connection connection;

    /**
     * The current login. null if not logged in
     */
    protected LoginInformation login;

    /**
     * The current transaction ID
     */
    protected int transactionid = 0;

    /**
     * The access controller
     */
    protected AccessController<T> controller = null;

    /**
     * The resource cache.
     */
    protected ResourceCache cache = new ResourceCache(this);

    /**
     * If true, the resource cache is used.
     */
    protected boolean caching = false;

    /**
     * Checks if the user is logged in. Throws an exception if there
     * is no login.
     * @throws NoLoginException if there is no login
     */
    protected void checkLogin() throws NoLoginException {
        logger.debug("Checking login...");
        if(login == null) {
            logger.error("No login available!");
            throw new NoLoginException();
        }
        logger.debug("Login checked and valid!");
    }

    @Override
    public void commit() throws DatabaseException {
        try {
            logger.debug("Executing commit...");
            connection.commit();
            transactionid = 0;
            logger.debug("Transaction committed and reset...");
        } catch (SQLException e) {
            logger.error("Error executing commit: " + e.getMessage());
            throw new DatabaseException(e);
        }
    }

    @Override
    public void close() throws DatabaseException {
        try {
            logger.debug("Closing connection...");
            connection.close();
        } catch (SQLException e) {
            logger.error("Error closing connection: " + e.getMessage());
            throw new DatabaseException("Could not close connection!");
        }
    }

    @Override
    public void beginTransaction() throws DatabaseException {
        checkLogin();

        if(transactionid != 0) {
            logger.error("Already in a transaction and trying to begin a new transaction!");
            throw new TransactionException();
        }
        logger.info("Starting new transaction.");

        try (PreparedStatement stmt = connection.prepareStatement("INSERT INTO transactions (user_id, \"timestamp\") VALUES " +
                    "(?, ?);", Statement.RETURN_GENERATED_KEYS)) {
            /**
             * Wir fügen einfach eine neue Zeile in der Tabelle transactions hinzu
             */
            stmt.setInt(1, login.uid);
            stmt.setTimestamp(2, new Timestamp(new Date().getTime()));
            stmt.execute();
            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if(generatedKeys.next()) {
                    transactionid = generatedKeys.getInt(1);
                } else {
                    throw new DatabaseException("No transaction ID returned!");
                }
            }
            logger.info("New transaction started. Transaction ID: " + transactionid);
        } catch(SQLException e) {
            throw new DatabaseException(e);
        }
    }

    /**
     * Initializes a new PSQLModel. Loads the database configuration from the specified
     * config file.
     * @param controller The access controller to use for this instance
     * @param configFile Path to config file. Must be a valid XML Postgresql file.
     * @throws DatabaseException
     */
    public PSQLModel(AccessController<T> controller, String configFile) throws DatabaseException {
        try {
            /**
             * Read the configuration file `config`
             */
            logger.debug("Reading config file...");

            this.config = JAXBUtil.unmarshall(new File(configFile), getJAXBContext(), Store.class);

            /**
             * Connect to postgresql
             */
            logger.debug("Establishing connection...");

            Postgresql psql = this.config.getPostgresql();

            init(controller, psql.getHost(), psql.getDatabase(), psql.getUsername(), psql.getPassword());
        } catch(DatabaseException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Exception: " + e.getMessage());
            e.printStackTrace();
            throw new DatabaseException("Unknown exception: " + e.getMessage());
        }
    }

    /**
     * Initializes a new PSQLModel. Loads the given database configuration.
     * @param controller
     * @param config
     * @throws DatabaseException
     */
    public PSQLModel(AccessController<T> controller, Store config) throws DatabaseException {
        try {
            this.config = config;

            /**
             * Connect to postgresql
             */
            logger.debug("Establishing connection...");

            Postgresql psql = this.config.getPostgresql();

            init(controller, psql.getHost(), psql.getDatabase(), psql.getUsername(), psql.getPassword());
        } catch(DatabaseException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Exception: " + e.getMessage());
            e.printStackTrace();
            throw new DatabaseException("Unknown exception: " + e.getMessage());
        }
    }

    /**
     * Initializes a new PSQLModel with the specified host, database, username and password.
     * @param controller
     * @param host
     * @param database
     * @param user
     * @param password
     * @throws DatabaseException
     */
    public PSQLModel(AccessController<T> controller, String host, String database,
            String user, String password) throws DatabaseException {
        init(controller, host, database, user, password);
    }

    /**
     * Initializes the connection with the specified host, database, username and password.
     * @param controller
     * @param host
     * @param database
     * @param user
     * @param password
     * @throws DatabaseException
     */
    private void init(AccessController<T> controller, String host, String database,
            String user, String password) throws DatabaseException {
        try {
            logger.debug("Establishing connection...");
            String url = "jdbc:postgresql://" + host + "/" +
                    database + "?user=" + user + "&password=" + password;

            /**
             * Do this kind of stuff in the ServletContextListener. Loading drivers is the web applications responsibility.
             */
//			Class.forName("org.postgresql.Driver").newInstance(); // this is important to work with JSF!

            connection = DriverManager.getConnection(url);
            connection.setAutoCommit(false);
            setAccessController(controller);
        } catch(SQLException e) {
            throw new NoConnectionException(config.getPostgresql().getHost(),
                    config.getPostgresql().getDatabase(), config.getPostgresql().getUsername());
        } catch (Exception e) {
            logger.error("Exception: " + e.getMessage());
            e.printStackTrace();
            throw new DatabaseException("Unknown exception: " + e.getMessage());
        }
    }

    @Override
    public Resource createResource(String type) {
        Resource resource = new Resource(type);
        return resource;
    }

    @Override
    public Resource reloadResource(Resource resource) throws DatabaseException {
        int id = resource.getId();
        String type = resource.getType();
        return getResource(type, id);
    }

    @Override
    public Resource getResourceByIdentifier(String identifier)
            throws DatabaseException {
        return getResource(getTypeFromResourceIdentifier(identifier),
                getIdFromResourceIdentifier(identifier));
    }

    @Override
    public Resource getResource(String type, int id) throws DatabaseException {
        return getResource(type, id, true);
    }

    public Resource getResource(String type, int id, boolean useCache) throws DatabaseException {
        if(useCache && caching) {
            return cache.handle(type, id);
        } else {
            ResourceQuery query = new ResourceQuery(type);
            query.addEqual(BasicDB.ID, id);
            ArrayList<Resource> results = getResources(query);

            if(results.size() == 0) {
                throw new DatabaseException("No results returned for type " + type + " and id " + id);
            }

            if(results.size() > 1) {
                throw new DatabaseException("Too many results returned for type " + type + " and id " + id);
            }

            return results.get(0);
        }
    }

    @Override
    public Integer countResources(ResourceQuery resourceQuery) throws DatabaseException {
        checkLogin();

        try {
            PreparedQuery<T> crit = prepareSQLQuery(resourceQuery.resultType, false, true, resourceQuery, false);

            if(!resourceQuery.isEmpty()) {
                crit.sqlQuery.addClause(Clauses.In(crit.idColumn, resourceQuery.prepareSQLQuery()));
            }

            Integer count = executeResourceCountQuery(crit);

            return count;
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public ArrayList<Resource> getResources(ResourceQuery resourceQuery)
            throws DatabaseException {
        return getResources(resourceQuery, false);
    }

    @Override
    ArrayList<Resource> getResources(ResourceQuery resourceQuery, boolean internal)
            throws DatabaseException {

        if(!internal) {
            checkLogin();
        }

        try {
            PreparedQuery<T> crit = prepareSQLQuery(resourceQuery.resultType, false, true, resourceQuery, internal);

            if(!resourceQuery.isEmpty()) {
                crit.sqlQuery.addClause(Clauses.In(crit.idColumn, resourceQuery.prepareSQLQuery()));
            }

            ArrayList<Resource> results = executeResourceQuery(crit);

            if(internal == false) {
                controller.filterProperties(results, crit.context, resourceQuery.resultType);
            }

            return results;
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public ArrayList<Resource> getResources(String type) throws DatabaseException {
        return getResources(new ResourceQuery(type));
    }

    /**
     * Checks if there is currently an open transaction.
     * @throws TransactionException If this model has not started a transaction.
     */
    protected void checkTransaction() throws TransactionException {
        if(transactionid == 0) {
            throw new NoTransactionException();
        }
    }

    @Override
    public void saveOrUpdateResource(Resource resource) throws DatabaseException {
        checkTransaction();

        if(resource.getId() != 0) {
            if(controller.canEditResource(resource)) {
                updateResource(resource);
            } else {
                rollback();
                throw new NotAuthorizedException();
            }
        } else {
            if(controller.canCreateResource(resource)) {
                saveHardResource(resource);
            } else {
                rollback();
                throw new NotAuthorizedException();
            }
        }
    }

    @Override
    void saveOrUpdateResourceInternal(Resource resource)
            throws DatabaseException {
        checkTransaction();

        if(resource.getId() != 0) {
            updateResource(resource);
        } else {
            saveHardResource(resource);
        }
    }

    /**
     * Updates a resources. Handles the history as well.
     *
     * @param resource The resource to update.
     * @throws DatabaseException
     */
    private void updateResource(Resource resource) throws DatabaseException {
        try {
            controller.prepareSaveResource(resource);

            // Wir brauchen nur die Spalten der Tabelle
            TableValues values = prepareValues(resource);
            TableDefinition def = DatabaseDefinitions.get(resource.getType());

            createHistory(resource);

            // Und anschließend das Update auf der "normalen" Tabelle durchführen
            values = prepareValues(resource);
            StringBuilder builder = new StringBuilder();
            builder.append("UPDATE \"" + def.table + "\" SET ")
                .append(values.getUpdateBindValues())
                .append(" WHERE id = " + resource.getId()).append(";");

            logger.debug("Update: " + builder.toString());

            try (PreparedStatement stmt = connection.prepareStatement(builder.toString())) {
                values.bindValues(stmt);
                stmt.execute();
            }

            for(Join j : def.getJoins()) {
                if(j.joinType == JoinType.N_TO_M_JOIN) {
                    builder = new StringBuilder();
                    builder.append("DELETE FROM \"").append(j.table).append("\" WHERE ");
                    builder.append(j.joinedColumn).append(" = ?");

                    try (PreparedStatement stmt = connection.prepareStatement(builder.toString())) {
                        stmt.setInt(1, resource.getId());
                        stmt.execute();
                    }

                    logger.debug("Update n-to-m join (DELETE): " + builder.toString());


                    for(Value v : resource.getProperties(j.property)) {
                        builder = new StringBuilder();
                        builder.append("INSERT INTO \"").append(j.table).append("\" (")
                            .append(DatabaseConstants.transactionsIdColumn + ", ").append(j.joinedColumn)
                            .append(", ").append(j.selectColumn).append(") VALUES (?, ?, ?);");

                        logger.debug("Update n-to-m join (INSERT): " + builder.toString());

                        try (PreparedStatement stmt = connection.prepareStatement(builder.toString())) {
                            stmt.setInt(1, transactionid);
                            stmt.setInt(2, resource.getId());
                            if(v instanceof Identifiable) {
                                stmt.setInt(3, ((Identifiable) v).getId());
                            } else {
                                throw new InvalidOperationException("Storing a non-identifiable value in a n-to-m relation");
                            }
                            stmt.execute();
                        }
                    }
                }
            }
        } catch (Exception e) {
            rollback();
            e.printStackTrace();
        }
    }

    /**
     * Copies the specified resource into the "_history" table.
     * @param resource
     * @throws SQLException
     * @throws PropertyNotDefined Usually this exception will not be thrown (it is impossible at this point),
     *     but the method used to get a TableValues instance throws it
     * @throws DatabaseException
     */
    private void createHistory(Resource resource) throws SQLException, PropertyNotDefined, DatabaseException {
        TableValues values = prepareValues(resource);
        TableDefinition def = DatabaseDefinitions.get(resource.getType());

        logger.info("Creating new history entry for resource " + resource.getValue());

        // Kopieren mit neuer ID in die Tabelle xxx_history
        StringBuilder builder = new StringBuilder("INSERT INTO \"").append(def.table)
            .append(DatabaseConstants.historyPostfix).append("\" (")
            .append(values.getInsertColumns()).append(", ").append(DatabaseConstants.oldIdColumn).append(") SELECT ")
            .append(values.getInsertColumns()).append(", id FROM \"").append(def.table)
            .append("\" WHERE id = ").append(resource.getId()).append(";");

        logger.debug("Insert: " + builder.toString());

        try (PreparedStatement stmt = connection.prepareStatement(builder.toString(), Statement.RETURN_GENERATED_KEYS)) {
            stmt.execute();
            stmt.close();
        }

        for(Join j : def.getJoins()) {
            if(j.joinType == JoinType.N_TO_M_JOIN) {
                builder = new StringBuilder();
                builder.append("INSERT INTO \"").append(j.table)
                    .append(DatabaseConstants.historyPostfix).append("\" (SELECT * FROM \"")
                    .append(j.table).append("\" WHERE ").append(j.joinedColumn).append(" = ?)");

                logger.debug("Update n-to-m join (INSERT HISTORY): " + builder.toString());

                try (PreparedStatement stmt = connection.prepareStatement(builder.toString())) {
                    stmt.setInt(1, resource.getId());
                    stmt.execute();
                    stmt.close();
                }
            }
        }
    }

    /**
     * Stores the specified resource as it is (no properties will be removed).
     * @param resource
     * @throws DatabaseException
     */
    private void saveHardResource(Resource resource) throws DatabaseException {
        try {
            controller.prepareSaveResource(resource);

            TableDefinition def = DatabaseDefinitions.get(resource.getType());
            TableValues values = prepareValues(resource);
            StringBuilder builder = new StringBuilder();
            builder.append("INSERT INTO \"").append(def.table).append("\" (")
                .append(values.getInsertColumns()).append(") VALUES (")
                .append(values.getInsertBindValues()).append(");");

            logger.debug("Saving resource " + resource);
            logger.debug("Insert: " + builder.toString());

            try (PreparedStatement stmt = connection.prepareStatement(builder.toString(), Statement.RETURN_GENERATED_KEYS)) {
                values.bindValues(stmt);
                stmt.execute();

                try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                    if(generatedKeys.next()) {
                        resource.setId(generatedKeys.getInt(1));
                    } else {
                        throw new DatabaseException("No ID returned from SQL-Statement!");
                    }
                }
            }

            for(Join j : def.getJoins()) {
                if(j.joinType == JoinType.N_TO_M_JOIN) {
                    for(Value v : resource.getProperties(j.property)) {
                        builder = new StringBuilder();
                        builder.append("INSERT INTO \"").append(j.table)
                            .append("\" (").append(DatabaseConstants.transactionsIdColumn)
                            .append(", ").append(j.joinedColumn).append(", ")
                            .append(j.selectColumn).append(") VALUES (?, ?, ?);");

                        logger.debug("Update n-to-m join (INSERT): " + builder.toString());

                        try (PreparedStatement stmt = connection.prepareStatement(builder.toString())) {
                            stmt.setInt(1, transactionid);
                            stmt.setInt(2, resource.getId());
                            if(v instanceof ResourceIdentifierLiteral) {
                                stmt.setInt(3, ((ResourceIdentifierLiteral) v).getId());
                            } else if(v instanceof Resource){
                                stmt.setInt(3, ((Resource) v).getId());
                            } else {
                                throw new InvalidOperationException("Storing a non-identifiable value in a n-to-m relation");
                            }
                            stmt.execute();
                        }
                    }
                }
            }
        } catch(DatabaseException e) {
            rollback();
            throw e;
        } catch (Exception e) {
            rollback();
            throw new DatabaseException(e);
        }
    }

    /**
     * Prepares the resource's values using the own class "TableValues". This is used to insert a new row
     * of data, or update it.
     * @param resource
     * @return
     * @throws PropertyNotDefined
     * @throws DatabaseException
     */
    @SuppressWarnings("unchecked")
    private TableValues prepareValues(Resource resource) throws DatabaseException {
        HashMap<String, ArrayList<Value>> values = (HashMap<String, ArrayList<Value>>) resource.get().clone();
        TableDefinition def = DatabaseDefinitions.get(resource.getType());

        TableValues dbValues = new TableValues();

        // Die Properties, die über einen Join eingelesen wurden, werden wir nicht im JSON-Datenfeld speichern
        for(Join j : def.getJoins()) {
            values.remove(j.property);
        }

        // Die ID und die Transaktions-ID sind unveränderlich und dürfen nicht ins data-Feld wandern
        values.remove(DatabaseConstants.idColumn);
        values.remove(DatabaseConstants.transactionsIdColumn);

        // Derzeitige Transaktions-ID festlegen!
        dbValues.createField(DatabaseConstants.transactionsIdColumn, transactionid, SQLColumnType.INTEGER);

        // Die Werte, für die eine Spalte definiert ist, bestimmen und festlegen
        for(TableColumn col : def.getColumns()) {
            if(values.containsKey(col.property)) {
                Value value = values.get(col.property).get(0);
                String v = values.get(col.property).get(0).getValue();
                switch(col.type) {
                case RESOURCE:
                    int id = getIdFromResourceIdentifier(v);
                    if(id == 0) {
                        throw new ResourceNotSavedException(resource.getValue());
                    }
                    dbValues.createField(col.column, id, col.type);
                    break;

                case INTEGER:
                    dbValues.createField(col.column, value.asInteger(), col.type);
                    break;

                case STRING:
                    dbValues.createField(col.column, v, col.type);
                    break;

                case TIMESTAMP:
                    dbValues.createField(col.column, new Timestamp(Long.parseLong(v)), col.type);
                    break;

                case BOOLEAN:
                    dbValues.createField(col.column, value.asBoolean(), col.type);
                    break;

                case FLOAT:
                    dbValues.createField(col.column, value.asFloat(), col.type);
                    break;

                default:
                    logger.error("Wrong SQLColumnType: " + col.type.toString());
                    throw new InvalidOperationException("Unknown SQLColumnType: " + col.type);
                }
            } else {
                if(col.isMandatory) {
                    logger.error("Mandatory property not defined: " + col.property);
                    throw new PropertyNotDefined(col.property);
                }
            }
            values.remove(col.property);
        }

        // The other properties go into the JSON "data" column.
        JsonObject data = ResourceUtils.createJsonObject(values);
        dbValues.createField(DatabaseConstants.dataColumn, data.toString(), SQLColumnType.JSON_OBJ);

        return dbValues;
    }

    /**
     * Fills the resource with the specified JSON object properties.
     * @param resource
     * @param data
     */
    private void fillData(AbstractResource resource, String data) {
        if(data != null) {
            JsonObject obj = (JsonObject) new JsonParser().parse(data);
            addValues(resource, obj);
        }
    }

    /**
     * Adds all properties defined in the JsonObject to the specified resource.
     * @param resource
     * @param obj
     */
    private void addValues(AbstractResource resource, JsonObject obj) {
        for(Entry<String, JsonElement> entry : obj.entrySet()) {
            addValues(resource, entry.getKey(), entry.getValue());
        }
    }

    /**
     * Adds the specified JsonElement to the resource, uses the specified property. This method is used to get
     * resources from the database.
     * @param resource the resource
     * @param property the property for the resource
     * @param element the GSON json object
     */
    private void addValues(AbstractResource resource, String property, JsonElement element) {
        if(element.isJsonPrimitive()) {
            if(element.getAsJsonPrimitive().isBoolean()) {
                resource.addProperty(property, new BooleanLiteral(element.getAsBoolean()));
            } else if(element.getAsJsonPrimitive().isNumber()) {
                resource.addProperty(property, element.getAsNumber());
            } else {
                String str = element.getAsJsonPrimitive().getAsString();
                try {
                    resource.addProperty(property, new TimestampLiteral(Timestamp.valueOf(str)));
                } catch(IllegalArgumentException e) {
                    if(str.endsWith(ResourceIdentifierLiteral.RESOURCE_IDENTIFIER)) {
                        resource.addProperty(property, ResourceIdentifierLiteral.fromString(str));
                    } else {
                        resource.addProperty(property, new StringLiteral(str));
                    }
                }
            }
        } else if(element.isJsonArray() && element.getAsJsonArray().size() > 0) {
            for(JsonElement e : element.getAsJsonArray()) {
                addValues(resource, property, e);
            }
        } else if(element.isJsonObject()) {
            JSONResource res = new JSONResource();
            addValues(res, element.getAsJsonObject());
            resource.addProperty(property, res);
        }
    }

    /**
     * Fills the resource dependent fields defined in the resources.xml configuration.
     * Every column that is defined in the resources.xml configuration will be filled here.
     *
     * @param resource the actual resource
     * @param resourceType the resource type
     * @param set the SQL result set
     * @param resourceSelection
     * @throws SQLException
     * @throws DatabaseException
     */
    private void fillFields(AbstractResource resource, String resourceType, ResultSet set,
                            SQLQuery sql, ResourceSelection resourceSelection, HashMap<String, HashMap<Integer, Resource>> resourceMap) throws SQLException, DatabaseException {
        for(Entry<SQLColumn, String> entry : resourceSelection.getPropertySelections().entrySet()) {
            SQLColumn column = entry.getKey();
            String prop = entry.getValue();

            if(prop == null) {
                continue;
            }

            switch(column.type) {
            case FLOAT:
            case JSON_FLOAT:
                resource.addProperty(prop, new NumberLiteral(set.getFloat(column.alias)));
                break;

            case INTEGER:
            case JSON_INT:
                resource.addProperty(prop, new NumberLiteral(set.getInt(column.alias)));
                break;

            case STRING:
            case JSON_STRING:
                resource.addProperty(prop, new StringLiteral(set.getString(column.alias)));
                break;

            case TIMESTAMP:
            case JSON_TIMESTAMP:
                resource.addProperty(prop, new TimestampLiteral(set.getTimestamp(column.alias)));
                break;

            case TIME:
            case JSON_TIME:
                resource.addProperty(prop, new TimeLiteral(set.getTime(column.alias)));
                break;

            case BOOLEAN:
            case JSON_BOOLEAN:
                resource.addProperty(prop, new BooleanLiteral(set.getBoolean(column.alias)));
                break;

            case RESOURCE:
                if(set.getInt(column.alias) != 0) {
                    if (resourceMap.containsKey(column.jsonProperty)) {
                        int id = set.getInt(column.alias);
                        if (resourceMap.get(column.jsonProperty).containsKey(id)) {
                            resource.addProperty(prop, resourceMap.get(column.jsonProperty).get(id));
                        } else {
                            Resource emptyResource = createResource(column.jsonProperty);
                            resourceMap.get(column.jsonProperty).put(id, emptyResource);
                            resource.addProperty(prop, emptyResource);
                        }
                    } else {
                        resource.addProperty(prop, new ResourceIdentifierLiteral(column.jsonProperty, set.getInt(column.alias)));
                    }
                }
                break;

            default:
                break;
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void login(String user, String password) throws DatabaseException {
        if(login != null) {
            logout();
        }

        int isActivated = 0;
        int failCounter = 0;

        try {
            // Get the salt from the database.
            String salt = null;

            try (PreparedStatement stmt = connection.prepareStatement("SELECT salt FROM users WHERE username = ?;")) {
                stmt.setString(1, user);
                try (ResultSet set = stmt.executeQuery()) {
                    if(set.next()) {
                        salt = set.getString("salt");
                    }
                }
            }

            if(salt == null) {
                throw new LoginFailedException();
            }

            /**
             * Hash the password.
             */
            String hashedPassword = hashPassword(password, salt);

            /**
             * Select the user with the hashedPassword.
             */
            try (PreparedStatement stmt = connection.prepareStatement("SELECT * FROM users WHERE username = ? AND password = ?;")) {
                stmt.setString(1, user);
                stmt.setString(2, hashedPassword);
                try (ResultSet set = stmt.executeQuery()) {
                    /**
                     * If there is a result, the credentials are valid. Otherwise they are not.
                     */
                    if(set.next()) {

                        login = new LoginInformation(set.getString(DatabaseConstants.usernameColumn),
                                set.getInt(DatabaseConstants.idColumn));

                        failCounter = set.getInt(DatabaseConstants.failCounterColumn);
                        isActivated = set.getInt(DatabaseConstants.isActivatedColumn);
                    }
                }
            }
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }

        /**
         * If the login was successful, reset the fail counter.
         */
        if(login != null) {
            /**
             * If however the user is *not* activated, invalidate the login and throw an exception.
             */
            if(isActivated == 0) {
                login = null;
                throw new UserNotActivatedException();
            }

            /**
             * If the credentials are valid, but the fail counter has been reached, invalidate
             * the login and throw an exception.
             */
            if(failCounter > config.getSecurity().getMaxFailCounter()) {
                login = null;
                throw new FailCounterException();
            }

            /**
             * Otherwise reset the fail counter.
             */
            try (PreparedStatement st = connection.prepareStatement("UPDATE users SET \""
                        + DatabaseConstants.failCounterColumn + "\" = 0 WHERE id = ?")) {
                st.setInt(1, login.uid);
                st.execute();
                connection.commit();
            } catch(SQLException e) {
                throw new DatabaseException(e);
            }

            /**
             * Let the access controller know that the login was successful.
             */
            controller.login(login);
        } else {
            /**
             * Increment the fail counter.
             */
            try (PreparedStatement st = connection.prepareStatement("UPDATE users SET \""
                        + DatabaseConstants.failCounterColumn + "\" = \"" + DatabaseConstants.failCounterColumn
                        + "\" + 1 WHERE username = ?")) {
                st.setString(1, user);
                st.execute();
                connection.commit();
            } catch(SQLException e) {
                throw new DatabaseException(e);
            }

            throw new LoginFailedException();
        }
    }

    @Override
    public void injectLogin(String user) throws DatabaseException {
        try (PreparedStatement stmt = connection.prepareStatement("SELECT * FROM users WHERE username = ?")) {
            stmt.setString(1, user);

            try (ResultSet set = stmt.executeQuery()) {
                if(set.next()) {
                    login = new LoginInformation(set.getString(DatabaseConstants.usernameColumn),
                                set.getInt(DatabaseConstants.idColumn));
                }
            }
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }

        if(login != null) {
            controller.login(login);
        } else {
            throw new LoginFailedException();
        }
    }

    @Override
    public void rollback() throws DatabaseException {
        try {
            this.connection.rollback();
            this.transactionid = 0;
        } catch (SQLException e) {
            logger.error("Error executing rollback: " + e.getMessage());
            throw new DatabaseException(e);
        }
    }

    @Override
    public void logout() {
        this.login = null;
        this.transactionid = 0;
        this.controller.logout();
    }

    /**
     * Returns the ID of the specified resource identifier.
     * @param str
     * @return
     */
    private int getIdFromResourceIdentifier(String str) {
        return Integer.parseInt(str.split(":")[1]);
    }

    /**
     * Returns the type of the specified resource identifier.
     * @param str
     * @return
     */
    private String getTypeFromResourceIdentifier(String str) {
        return str.split(":")[0];
    }

    /**
     * A helper class used for joins.
     *
     */
    private class JoinValues {
        private HashMap<Join, HashSet<Object>> values = new HashMap<>();

        public void addValue(Join join, Object value) {
            if(!values.containsKey(join)) {
                values.put(join, new HashSet<Object>());
            }

            values.get(join).add(value);
        }

        public HashSet<Object> get(Join join) {
            if(values.containsKey(join)) {
                return values.get(join);
            } else {
                return new HashSet<Object>();
            }
        }
    }

    @Override
    public void startCaching() {
        caching = true;
    }

    @Override
    public void stopCaching() {
        caching = false;
        cache.clear();
    }

    @Override
    public Resource createUser(String user, String password) throws DatabaseException {
        return createUser(user, password, PasswordHash.generateSalt());
    }

    @Override
    public Resource createUser(String user, String password, String salt) throws DatabaseException {
        String hashedPassword = hashPassword(password, salt);
        Resource ruser = importUser(user, hashedPassword, salt);
        if(password == null || password.equals("")) {
            ruser.setProperty(BasicDB.User.IsActivated, 0);
        }
        return ruser;
    }

    @Override
    public Resource importUser(String username, String hashedPassword, String salt) throws DatabaseException {
        logger.info("Creating new user: " + username);
        Resource resource = createResource(BasicDB.Type.User);
        resource.setProperty(BasicDB.User.Username, username);
        resource.setProperty(BasicDB.User.Password, hashedPassword);
        resource.setProperty(BasicDB.User.Salt, salt);
        resource.setProperty(BasicDB.User.IsActivated, 1);
        resource.setProperty(BasicDB.User.ActivationCode, generateNewUniqueActivationCode());
        resource.setProperty(BasicDB.User.FailCounter, 0);
        resource.setProperty(BasicDB.User.UserType, UserType.DEFAULT.toString());
        return resource;
    }

    private String generateNewUniqueActivationCode() throws DatabaseException {
        ResourceQuery query = null;
        String activationCode;
        do {
            query = new ResourceQuery(BasicDB.Type.User);
            activationCode = PasswordHash.generateRandomString(64, true);
            query.addEqual(BasicDB.User.ActivationCode, activationCode);
        } while(getResources(query, true).size() != 0);
        return activationCode;
    }

    @Override
    public Resource changeUserPassword(Resource user, String oldPassword, String newPassword)
            throws DatabaseException {

        String oldHashedPassword = hashPassword(oldPassword,
                user.getProperty(BasicDB.User.Salt).getValue());

        String newSalt = PasswordHash.generateSalt();
        String hashedPassword = hashPassword(newPassword, newSalt);

        if(!user.getProperty(BasicDB.User.Password).getValue().equals(oldHashedPassword)) {
            return null;
        }

        user.setProperty(BasicDB.User.Salt, newSalt);
        user.setProperty(BasicDB.User.Password, hashedPassword);
        user.setProperty(BasicDB.User.IsActivated, 1);
        user.setProperty(BasicDB.User.ActivationCode, "");
        user.setProperty(BasicDB.User.FailCounter, 0);
        return user;
    }

    @Override
    public Resource changeUserPasswordByAdmin(Resource user, String newPassword) throws DatabaseException {
        if(user.getId() == 0) {
            return null;
        }

        logger.info("Admin changes the password of user " + user.getProperty(BasicDB.User.Username).getValue());

        String newSalt = PasswordHash.generateSalt();
        String hashedPassword = hashPassword(newPassword, newSalt);

        user.setProperty(BasicDB.User.Salt, newSalt);
        user.setProperty(BasicDB.User.Password, hashedPassword);
        user.setProperty(BasicDB.User.IsActivated, 1);
        user.setProperty(BasicDB.User.ActivationCode, "");
        user.setProperty(BasicDB.User.FailCounter, 0);

        return user;
    }

    @Override
    public Resource forgotPassword(String username) throws DatabaseException {
        ResourceQuery query = new ResourceQuery(BasicDB.Type.User);
        query.addEqual(BasicDB.User.Username, username);
        Resource user = getResources(query, true).get(0);
        user.setProperty(BasicDB.User.ActivationCode, generateNewUniqueActivationCode());

        try (PreparedStatement st = connection.prepareStatement("UPDATE users SET \"activationCode\" = ?"
                    + " WHERE id = ?")) {
            st.setString(1, user.getProperty(BasicDB.User.ActivationCode).getValue());
            st.setInt(2, user.getId());
            st.execute();
            connection.commit();
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }

        return user;
    }

    @Override
    public Resource activateUser(String clearTextPassword, String activationCode) throws DatabaseException {
        if(activationCode == null || activationCode.equals("")) {
            return null;
        }

        // TODO: check if there is no open transaction.

        ResourceQuery query = new ResourceQuery(BasicDB.Type.User);
        query.addEqual(BasicDB.User.ActivationCode, activationCode);

        List<Resource> users = getResources(query, true);

        if(users.size() != 1) {
            return null;
        }
        String salt = PasswordHash.generateSalt();
        String hashedPassword = hashPassword(clearTextPassword, salt);
        Resource u = users.get(0);
        try (PreparedStatement st = connection.prepareStatement("UPDATE users SET \"isActivated\" = ?, \"activationCode\" = ?,"
                    + "\"password\" = ?, \"salt\" = ? WHERE \"id\" = ?")) {
            st.setInt(1, 1);
            st.setString(2, "");
            st.setString(3, hashedPassword);
            st.setString(4, salt);
            st.setInt(5, u.getId());
            st.execute();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return u;
    }

    @Override
    public void setAccessController(AccessController<T> controller) throws DatabaseException {
        if(controller == null) {
            throw new DatabaseException("PSQLModel needs a default controller. "
                    + "Use at least the default AccessController.");
        }

        this.controller = controller;
        this.controller.setModel(this);
    }

    @Override
    public String getUser() {
        if(login != null) {
            return login.name;
        } else {
            return null;
        }
    }

    @Override
    public int getUserId() {
        if(login != null) {
            return login.uid;
        } else {
            return 0;
        }
    }

    @Override
    public List<Transaction> getTransactions(Resource resource) throws DatabaseException {
        checkLogin();

        if(!controller.canAccessHistory(resource)) {
            logger.error("User " + getUser() + " is not authorized to access the history of the resource " + resource);
            throw new NotAuthorizedException();
        }

        String type = resource.getType();

        if(resource.getId() == 0) {
            throw new InvalidHistoryAccess();
        }

        List<Transaction> transactions = new ArrayList<Transaction>();

        try {
            TableDefinition def = DatabaseDefinitions.get(type);

            if(def == null) {
                throw new DatabaseException("Invalid access to type: " + type);
            }
            // First we get the old transactions from the history table
            SQLQuery oldTransaction = new SQLQuery(def.table + DatabaseConstants.historyPostfix);
            SQLColumn oldID = oldTransaction.getMainTable().getColumn(DatabaseConstants.oldIdColumn, SQLColumnType.INTEGER);
            SQLColumn tID = oldTransaction.getMainTable().getColumn(DatabaseConstants.transactionsIdColumn, SQLColumnType.INTEGER);
            oldTransaction.addClause(Clauses.Equal(oldID, new SQLIntValue(resource.getId())));
            oldTransaction.addSelection(tID);

            // Then we get the new transaction from the actual table
            SQLQuery newTransaction = new SQLQuery(def.table);
            SQLColumn newID = newTransaction.getMainTable().getColumn(DatabaseConstants.idColumn, SQLColumnType.INTEGER);
            SQLColumn newTID = newTransaction.getMainTable().getColumn(DatabaseConstants.transactionsIdColumn, SQLColumnType.INTEGER);
            newTransaction.addClause(Clauses.Equal(newID, new SQLIntValue(resource.getId())));
            newTransaction.addSelection(newTID);

            // We need the transactions from the transactions table
            SQLQuery tcrit = new SQLQuery(DatabaseConstants.transactionsTable);
            SQLColumn transactionID = tcrit.getMainTable().getColumn(DatabaseConstants.idColumn, SQLColumnType.INTEGER);
            tcrit.addSelection(transactionID);
            tcrit.addSelection(tcrit.getMainTable().getColumn("user_id", SQLColumnType.INTEGER));
            tcrit.addSelection(tcrit.getMainTable().getColumn(DatabaseConstants.timeColumn, SQLColumnType.TIMESTAMP));
            SQLTable mainTable = tcrit.getMainTable();

            tcrit.addClause(
                    Clauses.Or(
                            Clauses.In(tcrit.getMainTable().getColumn(DatabaseConstants.idColumn), oldTransaction),
                            Clauses.In(tcrit.getMainTable().getColumn(DatabaseConstants.idColumn), newTransaction)));

            tcrit.addOrder(new Order(tcrit.getMainTable().getColumn(DatabaseConstants.timeColumn), Order.Type.DESCENDING));

            try (PreparedStatement stmt = tcrit.prepare(connection); ResultSet set = stmt.executeQuery()) {
                while(set.next()) {
                    Transaction t = new Transaction(set.getInt(mainTable.getColumn(DatabaseConstants.idColumn).alias),
                            set.getInt(mainTable.getColumn("user_id").alias),
                            set.getTimestamp(mainTable.getColumn(DatabaseConstants.timeColumn).alias));
                    transactions.add(t);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return transactions;
    }

    @Override
    public Resource getResource(Resource resource, Transaction transaction) throws DatabaseException {
        checkLogin();

        if(!controller.canAccessHistory(resource)) {
            throw new NotAuthorizedException();
        }

        try {
            logger.debug("Checking normal table for history access");
            // First we check if the resource is in the "normal" table.
            PreparedQuery<T> crit = prepareSQLQuery(resource.getType(), false, false, null, true);
            crit.sqlQuery.addClause(Clauses.Equal(crit.idColumn, new SQLIntValue(resource.getId())));
            prepareHistoryQuery(crit, transaction);

            ArrayList<Resource> resources = executeResourceQuery(crit);

            // If the resource is *not* in the normal table, we check the history table.
            if(resources.size() == 0) {
                crit = prepareSQLQuery(resource.getType(), true, false, null, true);
                crit.sqlQuery.addClause(Clauses.Equal(crit.idColumn, new SQLIntValue(resource.getId())));
                prepareHistoryQuery(crit, transaction);

                resources = executeResourceQuery(crit);
            }

            // At this point we should have a resource
            if(resources.size() <= 0) {
                throw new InvalidHistoryAccess();
            }

            // Because the limit has been set to 1 (by prepareHistoryCritiera, and the order
            // is descending, the first resource is the resource we need.
            Resource target = resources.get(0);

            // Now we execute two queries for every join: first on the normal table
            // and then on the history table. We don't need the whole resource,
            // just the IDs
            for(Join j : crit.tableDefinition.getJoins()) {
                HashSet<Integer> resourceIDs = new HashSet<>();
                SQLQuery query = prepareHistoryJoinJoin(target, j, transaction, false);
                SQLColumn idCol = query.getMainTable().getColumn(j.selectColumn);

                try (PreparedStatement stmt = query.prepare(connection); ResultSet set = stmt.executeQuery()) {
                    while(set.next()) {
                        resourceIDs.add(set.getInt(idCol.alias));
                    }
                }

                query = prepareHistoryJoinJoin(target, j, transaction, true);
                idCol = query.getMainTable().getColumn(j.selectColumn);

                try (PreparedStatement stmt = query.prepare(connection); ResultSet set = stmt.executeQuery()) {
                    while(set.next()) {
                        resourceIDs.add(set.getInt(idCol.alias));
                    }
                }

                // After we have got all resources, we add them with resource identifiers to the
                // target resource.
                for(Integer i : resourceIDs) {
                    target.addProperty(j.property, new ResourceIdentifierLiteral(j.resourceType, i));
                }
            }

            return target;
        } catch(SQLException e) {
            throw new DatabaseException(e);
        }
    }

    /**
     * Prepares a SQLQuery for the specified resource, join and transaction. Used to get
     * a resource in a previous state.
     * @param resource
     * @param join
     * @param transaction
     * @param history
     * @return
     */
    private SQLQuery prepareHistoryJoinJoin(Resource resource, Join join, Transaction transaction, boolean history) {
        SQLQuery query = new SQLQuery(history ? join.table + DatabaseConstants.historyPostfix : join.table);
        SQLColumn idColumn = query.getMainTable().getColumn(join.selectColumn, SQLColumnType.INTEGER);
        SQLColumn transactionColumn = query.getMainTable().getColumn(DatabaseConstants.transactionsIdColumn, SQLColumnType.INTEGER);
        SQLColumn joinColumn = query.getMainTable().getColumn(join.joinedColumn, SQLColumnType.INTEGER);

        SQLTable transactions = query.getTable(DatabaseConstants.transactionsTable);
        SQLColumn tIDColumn = transactions.getColumn(DatabaseConstants.idColumn, SQLColumnType.INTEGER);
        SQLColumn tTimestampColumn = transactions.getColumn(DatabaseConstants.timeColumn, SQLColumnType.TIMESTAMP);

        query.addJoin(transactions, tIDColumn, transactionColumn);
        query.addClause(Clauses.LessOrEqual(tTimestampColumn,
                new SQLTimestampValue(transaction.time)));
        query.addClause(Clauses.Equal(joinColumn, new SQLIntValue(resource.getId())));

        query.addSelection(idColumn);
        return query;
    }

    /**
     * Prepares the query to be executed as a "history" query. That means that the query
     * returns the resources at the time of the specified transaction.
     * @param query
     * @param transaction
     */
    private void prepareHistoryQuery(PreparedQuery<T> query, Transaction transaction) {
        SQLTable transactions = query.sqlQuery.getTable(DatabaseConstants.transactionsTable);
        SQLColumn tIDColumn = transactions.getColumn(DatabaseConstants.idColumn, SQLColumnType.INTEGER);
        SQLColumn tTimestampColumn = transactions.getColumn(DatabaseConstants.timeColumn, SQLColumnType.TIMESTAMP);
        query.sqlQuery.addJoin(transactions, query.transactionColumn, tIDColumn);
        query.sqlQuery.addClause(Clauses.LessOrEqual(tTimestampColumn,
                new SQLTimestampValue(transaction.time)));

        query.sqlQuery.addOrder(new Order(tTimestampColumn, Order.Type.DESCENDING));
        query.sqlQuery.setLimit(1);
    }

    /**
     * Prepares a SQLQuery (encapsulated in a PreparedQuery) for the specified type using
     * the specified ResourceQuery. If history is true, this prepares a SQLQuery using the
     * "_history" table. If joins is true, it prepares the SQLQuery with all necessary joins.
     * If internal is true, the AccessController is ignored.
     * @param type
     * @param history
     * @return
     * @throws DatabaseException
     */
    private PreparedQuery<T> prepareSQLQuery(String type, boolean history, boolean joins,
            ResourceQuery query, boolean internal) throws DatabaseException {
        TableDefinition def = DatabaseDefinitions.get(type);

        if(def == null) {
            throw new DatabaseException("Invalid access to type: " + type);
        }

        HashMap<String, ResourceSelection> selections = new HashMap<>();

        /**
         * Create the SQLQuery and add all required clauses.
         */
        SQLQuery sqlQuery = new SQLQuery(history ? def.table + DatabaseConstants.historyPostfix : def.table);

        SQLColumn idCol = sqlQuery.getMainTable().getColumn(history ? DatabaseConstants.oldIdColumn : DatabaseConstants.idColumn, SQLColumnType.INTEGER);
        SQLColumn transactionCol = sqlQuery.getMainTable().getColumn(DatabaseConstants.transactionsIdColumn, SQLColumnType.INTEGER);
        SQLColumn dataCol = sqlQuery.getMainTable().getColumn(DatabaseConstants.dataColumn, SQLColumnType.JSON_OBJ);

        ResourceSelection primarySelection = new ResourceSelection(idCol, dataCol, transactionCol, true);

        for(TableColumn col : def.getColumns()) {
            SQLColumn scol = sqlQuery.getMainTable().getColumn(col.column, col.type, col.resourceType);
            sqlQuery.addSelection(col.property, scol);
            primarySelection.add(col.property, scol);
        }

        selections.put(type, primarySelection);

        /**
         * Add all joins, if "joins" is true.
         */
        HashMap<Join, SQLColumn> jColumns = new HashMap<>();

        if(joins && query.isFetchAdjacentResources()) {
            for(Join j : def.getJoins()) {
                SQLTable t = ResourceQuery.findTable(sqlQuery, type, j.resourceType, Collections.<ResourceQuery.Relation>emptyList(), false);
                SQLColumn scolid = t.getColumn(DatabaseConstants.idColumn, SQLColumnType.RESOURCE, j.resourceType);
                sqlQuery.addSelection(j.property, scolid);
                jColumns.put(j, scolid);
            }

            primarySelection.setJoinColumns(jColumns);

            for(String fetchType : query.getFetched()) {
                TableDefinition fetchDef = DatabaseDefinitions.get(fetchType);
                SQLTable table = ResourceQuery.findTable(sqlQuery, type, fetchType, Collections.<ResourceQuery.Relation>emptyList(), false);

                SQLColumn fetchTransactionCol = table.getColumn(DatabaseConstants.transactionsIdColumn, SQLColumnType.INTEGER);
                SQLColumn fetchDataCol = table.getColumn(DatabaseConstants.dataColumn, SQLColumnType.JSON_OBJ);
                SQLColumn fetchIdCol = table.getColumn(DatabaseConstants.idColumn, SQLColumnType.INTEGER);

                ResourceSelection selection = new ResourceSelection(fetchIdCol, fetchDataCol, fetchTransactionCol, false);

                sqlQuery.addSelection(fetchTransactionCol);
                sqlQuery.addSelection(fetchDataCol);
                sqlQuery.addSelection(fetchIdCol);

                for(TableColumn col : fetchDef.getColumns()) {
                    SQLColumn sqlCol = table.getColumn(col.column, col.type, col.resourceType);
                    selection.add(col.property, sqlCol);
                    sqlQuery.addSelection(col.property, sqlCol);
                }

                HashMap<Join, SQLColumn> joinColumns = new HashMap<>();

                for(Join j : fetchDef.getJoins()) {
                    SQLTable t = ResourceQuery.findTable(sqlQuery, type, j.resourceType, Collections.<ResourceQuery.Relation>emptyList(), false);
                    SQLColumn scolid = t.getColumn(DatabaseConstants.idColumn, SQLColumnType.RESOURCE, j.resourceType);
                    sqlQuery.addSelection(j.property, scolid);
                    joinColumns.put(j, scolid);
                    selection.add(j.property, scolid);
                }
                selection.setJoinColumns(joinColumns);

                selections.put(fetchType, selection);
            }
        }

        /**
         * Add the base selection fields.
         */
        sqlQuery.addSelection(DatabaseConstants.idColumn, idCol);
        sqlQuery.addSelection(DatabaseConstants.transactionsIdColumn, transactionCol);
        sqlQuery.addSelection(DatabaseConstants.dataColumn, dataCol);

        /**
         * If the access controller is available and internal is false,
         * initialize the context and let the AccessController configure the
         * query further.
         */
        AccessContext<T> context = null;
        if(!internal) {
            context = new AccessContext<T>();
            context.sql = sqlQuery;
            context.resourceQuery = query;
            controller.configureGet(context, type, selections);
        }

        return new PreparedQuery<>(sqlQuery, jColumns, selections, dataCol, idCol,
                transactionCol, context, type, def);
    }

    /**
     * Executes the specified PreparedQuery and returns a list of ready-to-use resources.
     * @param query
     * @return
     * @throws SQLException
     * @throws DatabaseException
     */
    private ArrayList<Resource> executeResourceQuery(PreparedQuery<T> query) throws SQLException, DatabaseException {
        HashMap<Integer, JoinValues> jvalues = new HashMap<>();
        ArrayList<Resource> target = new ArrayList<>();

        logger.debug("Executing: " + query.sqlQuery.construct());

        HashMap<String, HashMap<Integer, Resource>> resourceMap = new HashMap<>();

        for(Entry<String, ResourceSelection> t : query.fetchedSelections.entrySet()) {
            resourceMap.put(t.getKey(), new HashMap<Integer, Resource>());
        }

        try (PreparedStatement statement = query.sqlQuery.prepare(connection); ResultSet set = statement.executeQuery()) {
            while(set.next()) {

                /**
                 * For *every* fetched selection, we need to create the resource, fill the fields, etc, *once*.
                 */
                for (Entry<String, ResourceSelection> fetchedType : query.fetchedSelections.entrySet()) {
                    ResourceSelection selection = fetchedType.getValue();
                    int id = set.getInt(selection.idColumn.alias);

                    /**
                     * Ignore if the ID is NULL.
                     */
                    if(id == 0) {
                        continue;
                    }

                    HashMap<Integer, Resource> resSet = resourceMap.get(fetchedType.getKey());

                    /**
                     * If the resource does not exist yet, create it.
                     */
                    if (!resSet.containsKey(id) || resSet.get(id).getId() == 0) {
                        if(!resSet.containsKey(id)) {
                            resSet.put(id, createResource(fetchedType.getKey()));
                        }

                        Resource resource = resSet.get(id);
                        resource.setId(id);
                        resource.setProperty("id", id);
                        resSet.put(id, resource);

                        if (caching) {
                            cache.add(fetchedType.getKey(), id, resource);
                        }

                        try {
                            fillFields(resource, fetchedType.getKey(), set, query.sqlQuery, query.fetchedSelections.get(fetchedType.getKey()), resourceMap);
                            fillData(resource, set.getString(selection.dataColumn.alias));

                            if (selection.primary && query.context != null) {
                                controller.dataRow(resource, query.context, set);
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }

                        if(selection.primary) {
                            // Wir müssen diese Werte entfernen, sonst sind falsche oder doppelte Werte vorhanden!
                            for (TableDefinition.Join join : query.tableDefinition.getJoins()) {
                                resource.removeProperties(join.property);
                            }

                            jvalues.put(id, new JoinValues());
                        }
                    }



                    /**
                     * If the resource is the primary selection, handle the joins.
                     */
                    if(selection.primary) {
                        for (TableDefinition.Join join : query.joinColumns.keySet()) {
                            SQLColumn col = query.joinColumns.get(join);
                            Object obj = set.getObject(col.alias);
                            if (obj != null) {
                                jvalues.get(id).addValue(join, obj);
                            }
                        }
                    }
                }
            }
        }


        /**
         * Add the values from the joins.
         */
        for(Entry<Integer, Resource> entry : resourceMap.get(query.resourceType).entrySet()) {
            Integer id = entry.getKey();
            Resource res = entry.getValue();
            JoinValues values = jvalues.get(id);

            for(TableDefinition.Join join : query.tableDefinition.getJoins()) {
                for(Object value : values.get(join)) {
                    if(resourceMap.containsKey(join.resourceType)) {
                        res.addProperty(join.property, resourceMap.get(join.resourceType).get(value));
                    } else {
                        res.addProperty(join.property, new ResourceIdentifierLiteral(join.resourceType, (Integer) value));
                    }
                }
            }

            target.add(res);
        }

        return target;
    }

    private Integer executeResourceCountQuery(PreparedQuery<T> query) throws SQLException, DatabaseException {
        logger.debug("Executing: " + query.sqlQuery.construct());
        try (PreparedStatement statement = query.sqlQuery.prepare(connection, true); ResultSet set = statement.executeQuery()) {
            if (set.next()) {
                return set.getInt(1);
            } else {
                return 0;
            }
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public void executeStream(Reader reader) throws DatabaseException {
        String s;
        StringBuffer sb = new StringBuffer();

        BufferedReader br = new BufferedReader(reader);

        try {
            while ((s = br.readLine()) != null) {
                sb.append(s).append(System.lineSeparator());
            }
            br.close();

            executeSQL(sb.toString());
        } catch (Exception e) {
            logger.error("*** Error : " + e.toString());
            logger.error("*** ");
            logger.error("*** Error : ", e);
            logger.error("################################################");
            throw new DatabaseException(e);
        }
    }

    @Override
    public void executeSQL(String sql) throws DatabaseException {

        try (Statement st = connection.createStatement()) {
            logger.debug("Executing SQL: " + sql);
            st.execute(sql);
        } catch(SQLException e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public void executeFile(String filename) throws DatabaseException {
        try {
            InputStreamReader fr = new InputStreamReader(new FileInputStream(new File(filename)), StandardCharsets.UTF_8.displayName());

            executeStream(fr);

            fr.close();
        } catch (Exception e) {
            logger.error("*** Error : " + e.toString());
            logger.error("*** ");
            logger.error("*** Error : ", e);
            logger.error("################################################");
            throw new DatabaseException(e);
        }
    }

    @Override
    public ResourceIdentifierLiteral getCurrentUser() {
        return new ResourceIdentifierLiteral(BasicDB.Type.User, login.uid);
    }

    @Override
    public void selectRole(int roleid) throws DatabaseException {
        ResourceQuery query = new ResourceQuery(BasicDB.Type.Role);
        query.add(Criteria.Equal(BasicDB.Type.Role, BasicDB.ID, roleid));
        query.add(Criteria.Equal(BasicDB.Type.User, BasicDB.ID, login.uid));

        List<Resource> result = getResources(query, true);

        if(result.size() == 0) {
            throw new NotAuthorizedException();
        }

        Resource role = result.get(0);

        this.login.roleid = roleid;
        this.controller.roleSelected(role);
    }

    @Override
    public AccessController<T> getAccessController() {
        return this.controller;
    }

    /**
     * Installs all required tables.
     * @throws DatabaseException
     */
    public void installDatabase() throws DatabaseException {
        executeSQL(Configuration.get().generateSQL());
    }

    @Override
    public JSONResource getConfig(String name) throws DatabaseException {

        if(caching && cache.getConfig(name) != null) {
            return cache.getConfig(name);
        } else {

            SQLQuery query = new SQLQuery("configs");
            SQLTable table = query.getMainTable();
            SQLColumn colData = table.getColumn("data", SQLColumnType.JSON_OBJ);
            query.addSelection(colData);
            SQLColumn colName = table.getColumn("name", SQLColumnType.STRING);

            query.addClause(Clauses.Equal(colName, new SQLStringValue(name)));

            JSONResource result = null;

            try (PreparedStatement statement = query.prepare(connection); ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    String data = resultSet.getString(colData.alias);

                    if (data != null) {
                        result = new JSONResource();
                        JsonObject obj = (JsonObject) new JsonParser().parse(data);
                        addValues(result, obj);
                    }
                }
            } catch (SQLException e) {
                throw new DatabaseException(e);
            }

            if(caching) {
                cache.add(name, result);
            }

            return result;
        }
    }


    @Override
    public void saveConfig(String name, JSONResource config) throws DatabaseException {
        checkLogin();

        if(transactionid != 0) {
            throw new DatabaseException("You are in a transaction!");
        }

        if(!controller.canSaveConfig(name, config)) {
            throw new NotAuthorizedException();
        }

        String delete = "DELETE FROM \"configs\" WHERE \"name\" = ?";

        try {
            try (PreparedStatement st = connection.prepareStatement(delete)) {
                st.setString(1, name);
                st.execute();
            }

            String sql = "INSERT INTO \"configs\" (\"name\", \"data\") VALUES (?, CAST(? AS " + DatabaseConstants.getSQLJson()
                    + "));";

            try (PreparedStatement st = connection.prepareStatement(sql)) {
                st.setString(1, name);
                st.setString(2, ResourceUtils.createJsonElement(config).toString());
                st.execute();
            }

            connection.commit();

        } catch (SQLException e) {
            throw new DatabaseException(e);
        }

        return;
    }

    @Override
    public void deleteResource(Resource resource) throws DatabaseException {
        checkLogin();

        if(!controller.canDeleteResource(resource)) {
            throw new NotAuthorizedException();
        }



        try {
            logger.info("Deleting resource " + resource.getValue());

            TableDefinition def = DatabaseDefinitions.get(resource.getType());

            createHistory(resource);

            logger.info("Executing delete statement...");

            StringBuilder builder = new StringBuilder("DELETE FROM " + def.table + " WHERE id = ?");

            try (PreparedStatement stmt = connection.prepareStatement(builder.toString())) {
                stmt.setInt(1, resource.getId());
                stmt.execute();
            }

        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    @Override
    public Object executeAction(String name, Object... parameters)
            throws DatabaseException {
        checkLogin();

        if(!controller.canExecuteAction(name, parameters)) {
            throw new NotAuthorizedException();
        }

        return controller.executeAction(name, parameters);
    }


    /**
     * Hashes the specified password with the specified salt. Uses the security configuration settings.
     * @param input
     * @param salt
     * @return
     */
    protected String hashPassword(String input, String salt) {
        return PasswordHash.hashPassword(input, salt, config.getSecurity().getHashAlgorithm(),
                config.getSecurity().getRounds(), config.getSecurity().getPostfix());
    }

    /**
     *
     * @param query
     * @return
     * @throws DatabaseException
     */
    public PreparedStatement executeRaw(SQLQuery query) throws DatabaseException {
        try {
            return query.prepare(connection);
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    /**
     * The current login informations.
     *
     */
    public static class LoginInformation {

        /**
         * The user id
         */
        public final int uid;

        /**
         * The unique username
         */
        public final String name;

        /**
         * The current role ID
         */
        public int roleid;

        public LoginInformation(String name, int uid) {
            this.name = name;
            this.uid = uid;
        }

    }

}
